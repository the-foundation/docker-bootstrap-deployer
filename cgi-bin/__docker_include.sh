#!/bin/bash
#!/usr/bin/env bash
#
#DEBUGME=TRUE
#
#id -un;
test -e /home/$(id -un) || mkdir /dev/shm/home-$(id -un)
test -e /home/$(id -un) || export HOME=/dev/shm/home-$(id -un)
[[ "$(id -un)" = "www-data" ]] && export HOME=/var/www
source /etc/bashlib/_b4shl1b.sh
test -f /etc/outerenv && grep -q COMPOSE_ROOT /etc/outerenv && source /etc/outerenv
PRELOAD_MSG=""
html_header='
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="/tailwind.min.css" rel="stylesheet" >
<link href="/0.style.css"      rel="stylesheet">
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">';

_timestamper() {  while read "inline";do echo $(date "+%F_%T.%N %z |")"$inline";done ; } ;


_exit_with_msg()          { for item in "${@:2}" ;do echo -n "${item} "  ;done;echo ;exit "$1" ; } ;
_exit_with_msg_stdin()    { cat ;exit "$1"      ; } ;
_exit_with_errmsg()       { for item in "${@:2}" ;do echo -n "${item} " >&2 ;done;echo >&2 ;exit "$1" ; } ;
_exit_with_errmsg_stdin() { cat >&2 ;exit "$1"  ; } ;

_echo_redbox_m()          { echo '<div style="border-radius: 42px" class="ml-auto flex-grow mr-auto self-center text-xl lg:items-center text-center   bg-red-400  text-center  bg-opacity-75 w-full" >'"$@"'</div>' ; } ;
_echo_bluebox_m()         { echo '<div style="border-radius: 42px" class="ml-auto flex-grow mr-auto self-center text-xl lg:items-center text-center  bg-blue-400  text-center  bg-opacity-75 w-full" >'"$@"'</div>' ; } ;

_echo_redbox()            { echo '<div style="border-radius: 42px" class="ml-auto flex-grow mr-auto self-center text-3xl lg:items-center text-center  bg-red-400  text-center  bg-opacity-75 w-full" >'"$@"'</div>' ; } ;
_echo_bluebox()           { echo '<div style="border-radius: 42px" class="ml-auto flex-grow mr-auto self-center text-3xl lg:items-center text-center bg-blue-400  text-center  bg-opacity-75 w-full" >'"$@"'</div>' ; } ;

_echo_redbutton()         { echo '<button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">'"$@"'</button>'    ; } ;
_echo_bluebutton()        { echo '<button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">'"$@"'</button>'  ; } ;
#####
_redirmsg_prev()          { echo ${html_header}'<meta http-equiv="Refresh" content="7; URL='${HTTP_REFERER}'"></head><body>'$(_echo_redbox '<h1>Operation failed</h1><br><h3>'"$@")'</h3><hr><br>redirecting back in 7 seconds</body></html>' ; } ;
_redirmsg_prev_ok()       { echo ${html_header}'<meta http-equiv="Refresh" content="7; URL='${HTTP_REFERER}'"></head><body>'$(_echo_bluebox '<h1>Operation OK</h1><br><h3>'"$@")'</h3><hr><br>redirecting back in 7 seconds</body></html>' ; } ;

##find all public docker ports
_docker_get_all_ports()   { docker ps -a --format '{{.Ports}}'|sed 's/,/\n/g'|grep -v ^$|sed 's/^ \+//g;s/->.\+//g'|grep -e ^"0\.0\.0\.0" -e^"\[::\]"|sed 's/.\+]//g'|cut -d":" -f2 ; } ;

_format_block()             { maxlen=80;outbuffer=""; cat|while read inline;do
                                                                          outbuffer="$outbuffer $inline";
                                                                          [[ 20 -le "$(echo $outbuffer|wc -c )"   ]] && { echo $outbuffer|sed -e "s/.\{"$maxlen"\}/&\n/g" ; } ;
                                                                        done  ; } ;

_compose_up()             {
    cd "$1" || echo "PROJECT DIRECTORY "$1" not found"  |red
    cd "$1"; source .env ;
    #### DEFAULT CPU LIMIT, FORMULA HAS FOLLOWING RESULTS ( on 2 cores there is one spare for backup/proxies etc.)
    #### nCPU  | container cpu limit
    #### 1   | 1
    #### 2   | 1
    #### 3   | 2
    #### 4   | 2
    #### 5   | 3
    #### 6   | 3
    #### 7   | 4
    #### 8   | 4
    #### 12  | 6
    #### 16  | 8
    [[ -z "${CPU_LIMIT_MAX}" ]] && export CPU_LIMIT_MAX=$(($(nproc)*4/9+1))
    ## docker compose wil complain if we set max cpu's higher than the real number availabe cpu's
    UNDOTTED_CPU_LIMIT_MAX=${CPU_LIMIT_MAX//\.*/}
    [[ "${UNDOTTED_CPU_LIMIT_MAX}" -lt "$(nproc)" ]] && export CPU_LIMIT_MAX=$(($(nproc)*4/9+1))
    docker-compose up --build -d 2>&1 |grep -v -e ^$ -e "To deploy your application across the swarm" #| bash $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/_ansi2html.sh
 }

_compose_down() {
  cd "$1" || echo "PROJECT DIRECTORY "$1" not found"  |red
  cd "$1";docker-compose down 2>&1 #| bash $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/_ansi2html.sh
  }


_compose_pull() {
  echo "pulling compose containers for $1 "
    cd "$1" || echo "PROJECT DIRECTORY "$1" not found" |red
    cd "$1";docker-compose pull 2>&1  | _format_block #| bash $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/_ansi2html.sh
    }

_update_deployer_preset_library() {
    test -d /dev/shm/docker_container_lib || mkdir /dev/shm/docker_container_lib
    cat /etc/docker_library /etc/docker_library.domainadd |while read line;do
    echo -ne  "| getting  ${line}" .. "\t"  | sed 's/$/|/g' |sed 's/^/'$(echo {$line}|tr -dc 'a-zA-Z0-9'|md5sum|head -c 10)' /g'   ;
      giturl=${line/$(echo -e "\t")*/};
      #echo -n git = $giturl;
      targetfolder=/dev/shm/docker_container_lib/${giturl//*\//};
      echo " == "$targetfolder;

    (
    test -d $targetfolder ||   git clone $giturl $targetfolder 2>&1 | sed 's/$/|/g' |sed 's/^/'$(echo {$line}|tr -dc 'a-zA-Z0-9'|md5sum |head -c 10)' /g'
    test -d $targetfolder && { cd $targetfolder ; git pull     2>&1 | sed 's/$/|/g' |sed 's/^/'$(echo {$line}|tr -dc 'a-zA-Z0-9'|md5sum |head -c 10)' /g'   ;  }  ; )  & done 2>&1  | tee /dev/shm/docker-container-libraryupdate.log  #| sed 's/$/|/g'| tr -d '\n'
    echo -n ; } ;

#### END FUNCTIONS

if [ -z "${COMPOSE_ROOT}" ];then
   COMPOSE_ROOT=/opt/docker-compose/webprojects
    #PRELOAD_MSG=${PRELOAD_MSG}" "$()
fi
    test -d ${COMPOSE_ROOT} || _exit_with_msg 1 $(_echo_redbox "NO COMPOSE ROOT ${COMPOSE_ROOT} - PLEASE MAKE SURE THE MOUNTPOINT IS READ_WRITEABLEs")

_list_projects_raw()          {  find ${COMPOSE_ROOT}/ -mindepth 1 -maxdepth 1 -type d |sed 's/.\+\///g' ;  } ;

find /dev/shm/docker-envlist      -mmin -1 2>/dev/null | grep -q /dev/shm/docker || { envlist=$(ls -1 ${COMPOSE_ROOT}/*/*/.env 2>/dev/null );envlist=${envlist//${COMPOSE_ROOT}/} ; echo "$envlist" > /dev/shm/docker-envlist ; }
find /dev/shm/docker-composelist  -mmin -1 2>/dev/null | grep -q /dev/shm/docker || { composelist=$(ls -1 ${COMPOSE_ROOT}/*/*/docker-compose.yml 2>/dev/null );composelist=${composelist//${COMPOSE_ROOT}/} ; echo "$composelist" > /dev/shm/docker-composelist ; }
find /dev/shm/docker-customerlist -mmin -1 2>/dev/null | grep -q /dev/shm/docker || { _list_projects_raw > /dev/shm/docker-customerlist ; } &
find /dev/shm/docker-projectlist  -mmin -1 2>/dev/null | grep -q /dev/shm/docker || { cat /dev/shm/docker-composelist|while read entry;do rawentry=${entry//docker-compose.yml/};rawentry=${rawentry#/};rawentry=${rawentry%/}; echo $(dirname ${COMPOSE_ROOT}/$entry |sed 's/.\+\///g')  ${rawentry};done > /dev/shm/docker-projectlist; } &

_list_projects_raw()          {  find ${COMPOSE_ROOT}/ -mindepth 1 -maxdepth 1 -type d |sort | sed 's/.\+\///g' ; } ;
_list_projects_htmloptions()  {  find ${COMPOSE_ROOT}/ -mindepth 1 -maxdepth 1 -type d |sort | sed 's/.\+\///g' |sed 's/^.\+/<option value="\0" >\0<\/option>/g'  ; } ;


# Strip ANSI escape codes/sequences [$1: input string, $2: target variable]
function strip_escape_codes() {
    local input="${1//\"/\\\"}" output="" i char escape=0
    for (( i=0; i < ${#input}; ++i )); do         # process all characters of input string
        char="${input:i:1}"                       # get current character from input string
        if (( ${escape} == 1 )); then             # if we're currently within an escape sequence, check if
            if [[ "${char}" == [a-zA-Z] ]]; then  # end is reached, i.e. if current character is a letter
                escape=0                          # end reached, we're no longer within an escape sequence
            fi
            continue                              # skip current character, i.e. do not add to ouput
        fi
        if [[ "${char}" == $'\e' ]]; then         # if current character is '\e', we've reached the start
            escape=1                              # of an escape sequence -> set flag
            continue                              # skip current character, i.e. do not add to ouput
        fi
        output+="${char}"                         # add current character to output
    done
    eval "$2=\"${output}\""                       # assign output to target variable
}


js_live_love_log='<script src="/ansi_up.js" type="text/javascript"></script><script language="javascript" type="text/JavaScript">
      function getLog(log, lines) {
              var url = "/cgi-bin/_deploy.sh?log=" + log + "&lines=" + lines;
              request.open("GET", url, true);
              request.onreadystatechange = updatePage;
              request.send(null);
      };

      function tail(command,log,lines) {
              //if (command == "start") {
              //        document.getElementById("watchStart").disabled = true;
              //        document.getElementById("watchStop").disabled = false;
                      timer = setInterval(function() { getLog(log,lines);},2354);
              //} else {
              //        document.getElementById("watchStart").disabled = false;
              //        document.getElementById("watchStop").disabled = true;
              //        clearTimeout(timer);
              //}
      };

  function updatePage() {
  if (request.readyState == 4) {
      if (request.status == 200) {
          //var currentLogValue = request.responseText.split("\n");
          //eval(currentLogValue);
          //console.log(currentLogValue)
          var currentLogValue = request.responseText.replace(/(\r\n|\n|\r)/gm,"<br>");
          //var ansi_up = new AnsiUp;
          //var newhtml = ansi_up.ansi_to_html(currentLogValue);
          //document.getElementById("msgtxt").innerHTML = newhtml;
    document.getElementById("msgtxt").innerHTML = currentLogValue;
      if ( currentLogValue.search("OPERATION FINISHED") >=0)  {
      if (typeof FinishedConsole == "function") {  setTimeout(function() { FinishedConsole(); }, 235) }
       };
     }
    }
      };
  function FinishedConsole() { updatePage();clearTimeout(timer);document.getElementById("idlestatus").textContent="_ finished _ Closing Window in 10 seconds" ; setTimeout(function() { window.close();   document.location.href = "./../_stats-table.html" ; }, 9999)  ;  }  ;
    var request = (window.XMLHttpRequest) ? new XMLHttpRequest() : (window.ActiveXObject ? new window.ActiveXObject("Microsoft.XMLHTTP") : false);

    function trigger_logtail(logtarget){
    document.getElementById("idlestatus").innerHTML="<center><img class=rounded-full width=142px height=142px src=/images/_load_rainbow_circle.gif><br><h3> + Status: Deployment Started <br>lock your screen and get a drink  + </h3></center> " ;console.log("starting tail of " + logtarget);tail("start",logtarget,30) } ;
</script>'
