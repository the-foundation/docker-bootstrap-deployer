#!/bin/bash
_exitmsg() { echo "$1" ;exit 2}
read postline;
mypost=$(echo "${postline}"|sed 's/&/\n/g')
host=$(echo "${mypost}" |grep HOST=|cut -d= -f2-)

echo "docker-ssh-key-add"

keylength=$(ssh-keygen -lf /tmp/check_this_key.pub|cut -d" " -f1)
keytosmall=yes
echo "$keylength"|grep -e 4096 -e 8192 || _exitmsg "SORRY , NO WEAK KEYS ALLOWED"
