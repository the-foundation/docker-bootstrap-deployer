#!/bin/bash
read POST_STRING
echo "Content-type: text/plain"
echo  ""
if [[ "${HTTP_REFERER}" =~ "${HTTP_HOST}" ]]
then
# echo '[{"load":"'$(awk '/cpu /{print 100*($2+$4)/($2+$4+$5)}' /realproc/stat)'"}]'
 echo '[{"load":"'$(echo "$(which nproc &>/dev/null && nproc ||  (grep ^processor /proc/cpuinfo |wc -l) )" "$(cut /proc/loadavg -d" " -f2)" | awk '{printf  100*$2/$1 }')'"}]'
else
 echo '[{"permisson":"denied"}]'
fi
