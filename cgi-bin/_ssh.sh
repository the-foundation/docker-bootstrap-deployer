#!/bin/bash
#
#DEBUGME=TRUE
#
read POST_STRING
echo "Content-type: text/html"
echo ""
echo $POST_STRING >/tmp/loglog
source $( cd "$( dirname "${BASH_SOURCE[0]}"  >/dev/null )" 2>&1 && pwd )/__docker_include.sh

MSG=${PRELOAD_MSG}

_redirmsg()    { echo ${html_header}'<meta http-equiv="Refresh" content="7; URL=/cgi-bin/_ssh.sh?template=authorize"></head><body>'$(_echo_redbox '<h1>Operation failed</h1><br><h3>'"$1")'</h3><hr><br>redirecting back in 7 seconds</body></html>' ; } ;
_redirmsg_ok() { echo ${html_header}'<meta http-equiv="Refresh" content="7; URL=/cgi-bin/_ssh.sh?template=authorize"></head><body>'$(_echo_redbox '<h1>Operation OK</h1><br><h3>'"$1")'</h3><hr><br>redirecting back in 7 seconds</body></html>' ; } ;

_redirmsg_ssh()    { echo ${html_header}'<meta http-equiv="Refresh" content="7; URL=./../ssh"></head><body>'$(_echo_redbox '<h1>Operation failed</h1><br><h3>'"$1")'</h3><hr><br>redirecting back in 7 seconds</body></html>' ; } ;
_redirmsg_ssh_ok() { echo ${html_header}'<meta http-equiv="Refresh" content="7; URL=./../ssh"></head><body>'$(_echo_redbox '<h1>Operation OK</h1><br><h3>'"$1")'</h3><hr><br>redirecting back in 7 seconds</body></html>' ; } ;

[ "$DEBUGME" = "TRUE" ] && echo debug  precheck

## inital checks
[ ! -d "/dev/shm" ] && { _redirmsg $(_echo_redbox "Directory /dev/shm DOES NOT exists.")  exit 1 ;}
[ ! -w "/dev/shm" ] && { _redirmsg $(_echo_bluebox "/dev/shm NOT WRITABLE")         ; exit 1 ; }


if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=filezilla' ]] ;then

  [ ! -d "/etc/ssh_key_registry" ] && { _redirmsg "Directory /etc/ssh_key_registry DOES NOT exists."; exit 1  ; } ;
  [ ! -w "/etc/ssh_key_registry" ] && { _redirmsg "/etc/ssh_key_registry NOT WRITABLE"  exit 1 ; } ;

allcontainers=$(docker ps --filter status=running --format "{{.Names}}" |grep -v -e php_fpm$ -e php-fpm$ -e _memcached$ -e wp-fpm$ -e redirwww$ -e database$ -e mysql$ -e pgsql$ |grep -v ^docker-deployer )
allports=$(docker ps --filter status=running --format "{{.Names}} {{.Ports}}")
containerlist=$(echo "$allcontainers"|while read curcont;do echo "$allports"|grep "^${curcont} " |grep '>22/tcp' -q && echo ${curcont} ;done|sed 's/.\+/<option value="\0">\0<\/option>/g')
#usercount=$(cut -f1 /etc/ssh_key_registry/docker-deployer|sort -u |wc -l|cut -f1|sed 's/ .\+//g')
#keycount=$(wc -l /etc/ssh_key_registry/docker-deployer|cut -f1|sed 's/ .\+//g')
#userlist=$(cut -f1 /etc/ssh_key_registry/docker-deployer | while read basein ;do echo "$basein"|base64 -d ;done|awk '!x[$0]++'|sed 's/.\+/<option value="\0">\0<\/option>/g')
wait


echo ${html_header}'<body class="bg-blue-600">
<form action="/cgi-bin/_deploy.sh?action=filezilla" method="post" >
<!-- target="_blank">  -->
    <div style="border-radius: 42px" class="ml-auto flex-grow mr-auto self-center text-3xl lg:items-center text-center  bg-blue-400  text-center  bg-opacity-75 w-full" >
    <table style="border: 2px solid;min-width: 666px"  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-400 ml-auto mr-auto self-center border-blue-600 lg:items-center hover:bg-blue-800 text-white font-bold py-2 px-4 rounded-full text-3xl  ml-auto mr-auto self-center lg:items-center text-center  rounded-lg  text-center  bg-opacity-75" >
      <tbody>

      <tr><td><label  for="target">Container Name: </label></td><td>
      <!-- <input  class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " list="target" name="target" placeholder="Double-Click or Arrow-Keys" autocomplete="off"> -->
      <select style="border-radius:15px" class="block appearance-none w-full border border-blue-500 hover:border-blue-600 hover:bg-indigo-500 hover:text-white text-black px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" id=domain name="domain">'$containerlist' </select></td></tr>
      <tr><td rowspan=2><input type="hidden" id="action" name="action" value="filezilla">
      <center><button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" type="submit" value="Downloadfilezilla">Download filezilla Config</button></center></td>
      </tr>
    </tbody></table></div>
  </form></body></html>'

exit 0
fi

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=authorize' ]] ;then
  [ ! -d "/etc/ssh_key_registry" ] && { _redirmsg "Directory /etc/ssh_key_registry DOES NOT exists."; exit 1  ; } ;
  [ ! -w "/etc/ssh_key_registry" ] && { _redirmsg "/etc/ssh_key_registry NOT WRITABLE"  exit 1 ; } ;

allcontainers=$(docker ps --filter status=running --format "{{.Names}}" |grep -v -e php_fpm$ -e php-fpm$ -e _memcached$ -e wp-fpm$ -e redirwww$ -e database$ -e mysql$ -e pgsql$ |grep -v ^docker-deployer )
allports=$(docker ps --filter status=running --format "{{.Names}} {{.Ports}}")
containerlist=$(echo "$allcontainers"|while read curcont;do echo "$allports"|grep "^${curcont} " |grep -e '>22/tcp' -q && echo ${curcont} ;done|sed 's/.\+/<option value="\0">\0<\/option>/g')
usercount=$(cut -f1 /etc/ssh_key_registry/docker-deployer|sort -u |wc -l|cut -f1|sed 's/ .\+//g')
keycount=$(wc -l /etc/ssh_key_registry/docker-deployer|cut -f1|sed 's/ .\+//g')
userlist=$(cut -f1 /etc/ssh_key_registry/docker-deployer | while read basein ;do echo "$basein"|base64 -d ;done|awk '!x[$0]++'|sed 's/.\+/<option value="\0">\0<\/option>/g')
#wait


echo ${html_header}'<body><h4> <center><b>  ++  counters: (users: '$usercount' keys: '$keycount') ++ </b></center></h4>
<form action="/cgi-bin/_ssh.sh" method="post" >
<!-- target="_blank">  -->
    <div style="border-radius: 42px" class="ml-auto flex-grow mr-auto self-center text-3xl lg:items-center text-center  bg-blue-400  text-center  bg-opacity-75 w-full" >
    <table style="border: 2px solid;min-width: 666px"  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-200 ml-auto mr-auto self-center border-blue-600 lg:items-center hover:bg-blue-800 text-white font-bold py-2 px-4 rounded-full text-3xl  ml-auto mr-auto self-center lg:items-center text-center  rounded-lg  text-center  bg-opacity-75" >
      <tbody>
      <tr><td rowspan=2><input type="hidden" id="action" name="action" value="addtohost">
      <button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" type="submit" value="Add user key(s) to container">Add user key to container</button></td><td>
      <label class="text-blue" for="target">Container Name: </label></td><td>
      <!-- <input  class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " list="target" name="target" placeholder="Double-Click or Arrow-Keys" autocomplete="off"> -->
      <select style="border-radius:15px" class="block appearance-none w-full border border-blue-400 hover:border-blue-500 hover:bg-indigo-500 hover:text-white text-black px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" id=target name="target">'$containerlist' </select></td></tr>
      <tr><td> <label class="flex-grow lg:flex lg:items-center lg:w-auto ml-auto mr-auto self-center font-bold py-2 px-4 rounded-full"  for="uname"> SSH Key(s) of  : </label></td><td> <select  style="border-radius:15px" class="block appearance-none w-full  border border-blue-400 hover:border-blue-500 hover:bg-indigo-500 hover:text-white text-black px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" id="uname" name="uname">'$userlist' </select>  </td></tr>
    </tbody></table></div>
  </form><form action="/cgi-bin/_ssh.sh" method="post" >
  <!-- target="_blank">  -->
    <div style="border-radius: 42px" class="ml-auto flex-grow mr-auto self-center text-3xl lg:items-center text-center  bg-blue-400  text-center  bg-opacity-75 w-full" >
      <table style="border: 2px solid;min-width: 666px"  class="flex-grow lg:flex lg:items-center lg:w-auto bg-red-300 ml-auto mr-auto self-center lg:items-center hover:bg-blue-800 text-white font-bold py-2 px-4 rounded-full text-3xl  ml-auto mr-auto self-center lg:items-center text-center  rounded-lg  text-center " >
        <tbody>
        <tr><td> <label for="target">Container Name: </label></td><td>
        <!--<input  class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " list="target" name="target" placeholder="Double-Click or Arrow-Keys" autocomplete="off"> -->
        <select style="border-radius:15px" class="block appearance-none w-full border border-blue-400 hover:border-blue-500 hover:bg-indigo-500 hover:text-white text-black px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline"  id=target name="target">'$containerlist' </select></td><td rowspan=2><input type="hidden" id="action" name="action" value="removefromhost">
        <button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" type="submit" value="Remove user from container">Remove user from container</button></td></tr>
        <tr><td> <label class="flex-grow lg:flex lg:items-center lg:w-auto ml-auto mr-auto self-center font-bold py-2 px-4 rounded-full"  for="uname"> SSH Key(s) of : </label></td><td> <select style="border-radius:15px" class="block appearance-none w-full border border-blue-400 hover:border-blue-500 hover:bg-indigo-500 hover:text-white text-black px-4 py-2 pr-8 rounded shadow leading-tight focus:outline-none focus:shadow-outline" id="uname" name="uname">'$userlist' </select>  </td></tr>
      </tbody></table></div>
    </form></body></html>'

exit 0
fi

###########echo ${html_header}
[ "$DEBUGME" = "TRUE" ] && echo debug preparam

IFS='&' read -r -a SRV_POST <<< "${POST_STRING}"
action="none";
for index in "${!SRV_POST[@]}"; do
    ###############echo "$index ${SRV_POST[index]}" >> /tmp/post.log
    case "${SRV_POST[index]}" in
      action=addkey) action="addkey" ;;
      action=addtohost) action="addtohost" ;;
      action=removefromhost) action="removefromhost" ;;
    esac
done


## PARSE POST_STRING
saveIFS=$IFS
IFS='=&'
parm=($POST_STRING)
IFS=$saveIFS

declare -A param
for ((i=0; i<${#parm[@]}; i+=2)); do
#    param[${parm[i]}]=${parm[i+1]}; done
    ## Props: https://stackoverflow.com/a/3919908
    param[${parm[i]}]=$( _urldecode ${parm[i+1]} ); done

#echo after parse
## VERIFY ARRAY
#for index in "${!param[@]}"; do  echo "$index ${param[$index]} <br>"; done



#echo search  $(echo ${param[sshkey]}|cut -d" " -f2)
rawkey=$(echo ${param[sshkey]}|cut -d" " -f2)
[ "$DEBUGME" = "TRUE" ] && echo debug  $rawkey

### action was parsed , verify if files exist , user entered anything and if values seem ok
validvalues="no"


##Check /etc/ssh_key_registry exists and is writable
[ ! -d "/etc/ssh_key_registry" ] && { _redirmsg "Directory /etc/ssh_key_registry DOES NOT exists." ;exit 1 ; }
[ ! -w "/etc/ssh_key_registry" ] && { _redirmsg "/etc/ssh_key_registry NOT WRITABLE" ;exit 1 ; }
##generate blank file if necessary
test -f /etc/ssh_key_registry/docker-deployer || touch /etc/ssh_key_registry/docker-deployer
[ "$DEBUGME" = "TRUE" ] && echo debug "search $rawkey" >> /tmp/loglog
###############################################################################################<br>raw entry : <code> "$(grep  "${rawkey}" /etc/ssh_key_registry/docker-deployer|sed 's/\t/ /g')"</code><br>checksum : <code>"$(grep  "${rawkey}" /etc/ssh_key_registry/docker-deployer |cut -f2- |ssh-keygen -lf /dev/stdin |sed 's/\t/ /g' )"</code><hr><br>")

[ "$DEBUGME" = "TRUE" ] && echo debug  caseaction $action

case "${action}" in
    addkey)
            #check default values
            [ "$DEBUGME" = "TRUE" ] && echo debug  check ${param[uname]} and ${param[sshkey]}
            echo "$POST_STRING" |grep -q -e 'uname=&' -e 'sshkey=&' -e "uname=User+Name" -e "sshkey=ssh-rsa\.\.\." && { _redirmsg_prev $(_echo_redbox "You have to enter name and key"); exit 1 ; }
            #check key existence
            cat /etc/ssh_key_registry/docker-deployer | grep -q "$rawkey"   && {
              [ "$DEBUGME" = "TRUE" ] && echo debug  redir key exists"<br>"
              _echo_redbox "<br><h3>Key already added as user .. </h3>";
              _echo_redbox "Key is assigned to  :"$(grep  "$rawkey" /etc/ssh_key_registry/docker-deployer |cut -f1|base64 -d |sed 's/$/,/g'|tr -d '\n' )
              _redirmsg_ssh "redirecting...";exit 1 ; };
            [ "$DEBUGME" = "TRUE" ] && echo debug  check username ${param[uname]}
            ##CHECK Username
            if ! [[ "${param[uname]}" =~ [^a-zA-Z0-9\ \-_] ]]; then
                [ "$DEBUGME" = "TRUE" ] && echo debug  ok
                MSG=${MSG}"<br>username ok<br>"
                #### if ↓ force not set  ###############        ##               see if user exists
                echo ${param[force]} |grep -v -q ^on  &&  {
                  grep "$(echo ${param[uname]}|base64|tr -d '\n')" /etc/ssh_key_registry/docker-deployer -q && {
                  [ "$DEBUGME" = "TRUE" ] && echo debug  user exists but no force set
                  _redirmsg_prev $(_echo_redbox "User already registered , use FORCE to add multiple keys ") ;exit 1 ;
                  #retval=$?;if [ "$retval" -ne 0 ];then exit $retval;fi;
                  echo -n ; } ;
                echo -n ; } ;
                [ "$DEBUGME" = "TRUE" ] && echo debug  afterforcetest
            else
                [ "$DEBUGME" = "TRUE" ] && echo debug  failed
                _redirmsg_prev  "Username is not alphanumeric (0-9a-zA-Z _ -)"
            fi

            [ "$DEBUGME" = "TRUE" ] && echo debug  check key #${param[sshkey]}
            echo ${param[sshkey]} |  grep -q -e ssh-rsa -e ssh- || {
              _redirmsg_prev $(_echo_redbox "ssh key does not begin with ssh- ...") ; exit 1 ; } ;

            keylength=$(echo ${param[sshkey]} | _ssh_keylength);
            [ "$DEBUGME" = "TRUE" ] && echo debug  length $keylength
            case "${param[sshkey]}" in
              *ssh-rsa* )
                [ "$DEBUGME" = "TRUE" ] && echo debug  key
                if [ "$(date +%Y)" -gt 2022 ]; then
                    [ "$DEBUGME" = "TRUE" ] && echo debug  length after 2021
                    echo "${keylength}"  |grep  -q -e 8192 -e 16384  ||  {
                                          [ "$DEBUGME" = "TRUE" ] && echo debug  insecure key
                                          _redirmsg_prev $(_echo_redbox "No insecure keys allowed (RSA<8192) : $keylength") ; exit 1 ;
                                          #retval=$?; if [ "$retval" -ne 0 ];then exit $retval;fi
                                          echo -n ; } ;
                else
                    [ "$DEBUGME" = "TRUE" ] && echo debug  keylength ${keylength}
                    echo ${param[force]} |grep -q ^on$ ||   {
                            count=$(echo "${keylength}"  |grep  -e 4096 -e 8192 -e 16384|wc -l)
                            if [ "$count" = "0" ]   ;then
                              [ "$DEBUGME" = "TRUE" ] && echo debug  failed
                                _echo_redbox "Key too weak ${keylength} (counted $count)"
                                _redirmsg_prev $(_echo_redbox "No insecure keys allowed (RSA<8192) : ${keylength}") ;exit 1  ;
                              #retval=$?; if [ "$retval" -ne 0 ];then exit $retval;fi;
                                echo -n ;
                            fi
                              echo -n  ; } ;
                    [ "$DEBUGME" = "TRUE" ] && echo debug  afterkeylength
                fi
              echo -n ;;
              *ssh-ed25519* )
                MSG="{MSG}"'<div> Be sure that your containers have dropbear > 2020.03 <br>'
              echo  -n ;;
              *) _exit_with_msg 1 $(_redirmsg_ssh $(_echo_redbox "SSH-RSA8192 | SSH-ED25519 only , please use a passphrase or full disk encryption and <b>ssh-keygen -t rsa -b 8192</b>")) ;retval=$?; if [ "$retval" -ne 0 ];then exit $retval;fi ;;
            esac

            [ "$DEBUGME" = "TRUE" ] && echo debug  store key

            ## STORE KEY
            (echo -n $(echo "${param[uname]}" |base64 |tr -d '\n');echo -ne  '\t';echo $(echo "${param[sshkey]}" |tr -d '\n')) >> /etc/ssh_key_registry/docker-deployer
    echo -n ;;
    addtohost)
        targetkeys=$(docker exec -t ${param[target]} /bin/sh -c "test -d /var/www/.ssh/ && test -f /var/www/.ssh/authorized_keys && cat /var/www/.ssh/authorized_keys")
        [ "$DEBUGME" = "TRUE" ] && echo debug  targetkeys_length $(echo "$targetkeys"|wc -l)
        keyname=$(echo ${param[uname]}|base64 |tr -d '\n')
        ### AUTHORIZE
        #_echo_bluebox "adding ssh key(s) FOR ${param[uname]} TO ${param[target]}";
        [ "$DEBUGME" = "TRUE" ] && echo debug  testforssh"<br>"
        _docker_container_has_ssh "${param[target]}" || { _redirmsg_prev $(_echo_redbox "container is no ssh container") ; exit 1 ; } ;
        [ "$DEBUGME" = "TRUE" ] && echo debug  create authkey"<br>"
        operation_result=$(          docker exec -t "${param[target]}" /bin/sh -c "test -d /var/www/.ssh/||mkdir -p /var/www/.ssh/ ;test -f /var/www/.ssh/authorized_keys ||  ( touch /var/www/.ssh/authorized_keys;chown www-data /var/www/.ssh/authorized_keys;chmod go-rwx /var/www/.ssh -R ;chmod u+rwx /var/www/.ssh;chmod u-w  /var/www/.ssh/authorized_keys  ) ; "  2>&1        )
        [ "$DEBUGME" = "TRUE" ] && echo debug  aftertest
        _echo_bluebox "creating authorized keys if non existent..  $operation_result"
        [ "$DEBUGME" = "TRUE" ] && echo debug  keynameloop "$keyname"
        grep "$keyname" /etc/ssh_key_registry/docker-deployer |cut -f2- |while read insertline;do
          #[ "$DEBUGME" = "TRUE" ] && echo debug  insertline $(echo $insertline |head -c 50)"<br>"
        searchkey=$(echo "${insertline}" |cut -d" " -f2)
        #insertkey=$(echo "${insertline}" |sed 's/\//\\\//g')
        insertkey=$(echo "${insertline} ${param[uname]}")
        [ "$DEBUGME" = "TRUE" ] && echo debug  searchkey $(echo "$searchkey" |tail -c 30 )"<br>"
        [ "$DEBUGME" = "TRUE" ] && echo debug  insertkey $(echo "$insertkey" |tail -c 30 )"<br>"
        [ "$DEBUGME" = "TRUE" ] && { echo debug  test search in target ;echo "${targetkeys}" | grep -q "${searchkey}" && echo OK;echo "<br>" ; } ;
        echo "${targetkeys}" | grep -q "${searchkey}" && ( _echo_redbox "key "$(echo "${searchkey}"|head -c 8)"..."$(echo "${searchkey}"|tail -c 8)" already in container" );
        [ "$DEBUGME" = "TRUE" ] && echo debug  test2"<br>"
        echo "${targetkeys}" | grep -q "${searchkey}" || {
                                  [ "$DEBUGME" = "TRUE" ] && echo debug  inserting "$(echo $insertkey|head -c 22) <br>"
                                  #echo "${insertkey}" | grep -e ^ssh-rsa -e ^ssh-ed25519;echo "<br>";
                                  echo "${insertkey}" | grep -e ^ssh-rsa -e ^ssh-ed25519 | docker exec -i "${param[target]}" /bin/sh -c "tee -a /var/www/.ssh/authorized_keys" 1>&2  ; } ;
        done
    echo -n ;;

    removefromhost)
        targetkeys=$(docker exec -t ${param[target]} /bin/sh -c "test -d /var/www/.ssh/ && test -f /var/www/.ssh/authorized_keys && cat /var/www/.ssh/authorized_keys")
        [ "$DEBUGME" = "TRUE" ] && echo debug targetkeys #"$targetkeys"
        if [ -z "$targetkeys" ]; then
          _echo_redbox "targetkeys empty, container has no authorized_keys (file)";
        else
          for rawkey in $(grep $(echo "${param[uname]}" |base64 |tr -d '\n') /etc/ssh_key_registry/docker-deployer|cut -f2- |cut -d" " -f2) ;do
            [ "$DEBUGME" = "TRUE" ] && echo debug rawkey "$rawkey" |tail -c 20
            escapedkey=$(echo "${rawkey}"|sed 's/\//\\\//g')
            echo "${targetkeys}" |grep -q "${rawkey}" || MSG=${MSG}$(_echo_redbox " key "$(echo "${rawkey}"|head -c 8)"..."$(echo "${rawkey}"|tail -c 8)" not in container" )
            echo "${targetkeys}" |grep -q "${rawkey}" && docker exec -t ${param[target]} /bin/sh -c "sed -i '/"${escapedkey}"/d' /var/www/.ssh/authorized_keys" >&2 && docker exec -t ${param[target]} /bin/sh -c "test -d /var/www/.ssh/ && test -f /var/www/.ssh/authorized_keys && cat /var/www/.ssh/authorized_keys" |grep -q "${rawkey}" && MSG=${MSG}$(_echo_bluebox "adding seemed successful")
          done
        fi
    echo -n ;;

esac

[ "$DEBUGME" = "TRUE" ] && echo debug  fin

###

_redirmsg_prev_ok "${MSG}"' '"$(_echo_bluebox 'Operation seems OK')" ;
exit 0
#echo $action
