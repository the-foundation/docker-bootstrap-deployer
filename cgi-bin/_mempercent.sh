#!/bin/bash
read POST_STRING
echo "Content-type: text/plain"
echo  ""
if [[ "${HTTP_REFERER}" =~ "${HTTP_HOST}" ]]
then
# echo '[{"memperc":"'$(awk '/MemFree/{free=$2} /MemTotal/{total=$2} END{print (free*100)/total}' /realproc/meminfo)'"}]'
 echo '[{"memperc":"'$(echo $(grep -e MemTotal -e MemFree -e Buffers -e Cached /proc/meminfo|sed 's/\([0-9]\+\) kB/\1/g;s/\( \|\t\)//g;'|cut -d: -f2)|awk '{print 100-100*($2+$3+$4)/$1}')'"}]'
else
 echo '[{"permisson":"denied"}]'
fi
