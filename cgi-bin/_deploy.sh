#!/bin/bash
#
#DEBUGME=TRUE
#
read POST_STRING
## some actions return json
echo "${REQUEST_URI}" | grep -e "action=verifydomain" -e "action=filezilla" -e "action=verifyspf" -q || { echo "Content-type: text/html" ;echo "" ; } ;
echo $POST_STRING >/tmp/loglog
source $( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/__docker_include.sh

env_files_as_html_select() {
  cat /dev/shm/docker-envlist |grep -v ^$|sort -n | while read envplace;do
    echo '<option value="'$(echo $envplace |sed 's/^\///g;s/\/.env$//g;s/.\+\///g')'" >'$(echo $envplace |sed 's/^\///g;s/\/.env$//g;s/\// -> /g')'</option> ' ;
  done
}

http_env_files_as_html_select() {
  find "${COMPOSE_ROOT}" -name "\.env" |grep -v ^$|sort -n | while read envplace;do
  grep -q "VIRTUAL_PROTO" "${envplace//\.env/docker-compose.yml}" &&  grep -q "VIRTUAL_HOST" "${envplace//\.env/docker-compose.yml}" &&  {
    envplace=${envplace//${COMPOSE_ROOT}/}
    echo '<option value="'${envplace}'" >'$(echo $envplace |sed 's/^\///g;s/\/.env$//g;s/\// -> /g')'</option> ' ;
  echo -n ; } ;
  done
}

_container_presets_available() { cat /etc/docker_library|  sed 's/^\(.\+\)\t\(.\+\)/<option value="\1" >\2<\/option>/g' ; } ;

targetactions_as_html_select() { echo  '
                                        <option value="loglive" >Show logs(live)</option>
                                        <option value="logdownlad" >Download logs</option>
                                        <option value="stop" >Stop</option>
                                        <option value="stopremove" >Stop And Remove</option>
                                        ' ; }

############################################

javascript_validate_functions='<script>

function fillMailUser(username) {
document.getElementById("MAIL_USERNAME").value=username;
}

function fillMailHost(hostname) {
  document.getElementById("MAIL_HOST").value=hostname;
}
function hideElement(element) {
element.style.display="none";
}
function noFunction(e){
    e.preventDefault();// will stop the form submission
    // rest of the code
}
function dontSubmit(e){
    e.preventDefault();// will stop the form submission
    // rest of the code
}

function validateDomain(DomainField){
console.log("test domain:" + DomainField.value);

var regsub = /^([A-Za-z0-9\-\.])+\.([A-Za-z]{2,32})$/;
var reg = /^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/ ;
var regwww = /^www\./;
var seqdot = /\.\./;
var emptyregex= /^$/;

if (emptyregex.test(DomainField.value) == true )
{ console.log("file:empty");return false;
}

if (seqdot.test(DomainField.value) == true )
{ console.log("f:double dot");
alert("domain schema invalid (..)");      return false;
}
  if (regwww.test(DomainField.value) == true )
  { console.log("f:www. prefixed");
  alert("domain schema invalid (www.)");      return false;
 }
  if (reg.test(DomainField.value) == false)
  { console.log("f:no tld");
    if (regsub.test(DomainField.value) == false)
    {
      console.log("f:no sub.domain");
      alert("domain schema invalid");      return false;
    }
  }
  return true;
}
function validateEmail(emailField){

 var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,32})$/;
 if (reg.test(emailField.value) == false)
 {
     alert("Invalid Email Address");   return false;
 }
 return true;
}

function validateEmailByID (domID) {
    if(typeof(document.getElementById(domID)) != 'undefined' && document.getElementById(domID) != null){
          validateEmail(document.getElementById(domID))
      } else{
      console.log("no element: " + domID)
      }
}

function validateMailPass() {
if (document.getElementById("MAIL_PASSWORD").value == "")
 {
   if ( document.getElementById("MAIL_USERNAME").value  != "InternalNoTLSNoAuth" )
   {  alert("Invalid Email Password");   return false; }
 }
return true
};


function SPFstatusToElement(testDomain) {

document.getElementById("spfresult").innerText=".......\nStarting\n  ... ..."
// Create XMLHttpRequest object.
var oXHR = new XMLHttpRequest();
var jdata ;
// Initiate request.
oXHR.onreadystatechange = reportSPF;
oXHR.open("GET", "/cgi-bin/_deploy.sh?action=verifyspf&domain=" + testDomain, true);  // get json file.
oXHR.send();
document.getElementById("spfresult").innerText="..SPF..\nRequesting\n  ... ..."

function reportSPF() {
    if (oXHR.readyState == 4) {		// Check if request is complete.
        console.log(this.responseText);
        // Create an HTML table using response from server.
        writespfstatus(JSON.parse(this.responseText));
    }
}

function writespfstatus(input) {
    console.log(input.success);
    if (input.success == true)
          {
          document.getElementById("spfresult").innerText="+|  SPF OK  |+";
          }
    else {
          document.getElementById("spfresult").innerText="| SPF failed,                          |\n |  you may not send via internal SMTP |\n" + input.msg.replace("/|/g", "|\n");
         }
    }

}
function DNSstatusToElement(testDomain) {

document.getElementById("dnsresult").innerText=".......\nStarting\n  ... ..."
// Create XMLHttpRequest object.
var oXHR = new XMLHttpRequest();
var jdata ;
// Initiate request.
oXHR.onreadystatechange = reportDNS;
oXHR.open("GET", "/cgi-bin/_deploy.sh?action=verifydomain&domain=" + testDomain, true);  // get json file.
oXHR.send();
document.getElementById("dnsresult").innerText="..DNS..\nRequesting\n  ... ..."

function reportDNS() {
    if (oXHR.readyState == 4) {		// Check if request is complete.
        console.log(this.responseText);
        // Create an HTML table using response from server.
        writeDNSstatus(JSON.parse(this.responseText));
    }
}

function writeDNSstatus(input) {
    console.log(input.success);
    if (input.success == true)
          {
          document.getElementById("dnsresult").innerText="+|  DNS OK  |+";
          }
    else {
          document.getElementById("dnsresult").innerText="| DNS failed,                          |\n | dns check for host failed|\n"+input.msg.replace("/|/g", "\n|");
         }
    }

}
</script>'

MSG=''

#### PROXY DOMAIN(socat) CHANGES TEMPLATE
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=domain-add' ]] ;then



echo "$html_header" "$javascript_validate_functions" '</head><body>
  <form id="createform" accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?template=proxydomain_create" target="change-iframe" method="post" autocomplete="off">

    <div class=" border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
    <div class=" border border-opacity-75 border-blue-500 bg-opacity-75 bg-black text-white flex rounded-md"> ... <h3><label for="containertype">1st step: select container type ,project space and domain</label></h3></div>
    <center>
    <table class=" border border-opacity-75 border-black rounded-md text-3xl">
    <tr><td></td><td></td><td>Move your mouse <br>over the button<br> to verify the domain</td><td><button  class=" absolute top-10 right-10  flex-grow lg:flex  lg:w-auto bg-blue-600  hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" onMouseOver="DNSstatusToElement(document.getElementById('"'domain'"').value);SPFstatusToElement(document.getElementById('"'domain'"').value);validateDomain(document.getElementById('"'domain'"'));" onClick="hideElement(this);hideElement(document.getElementById('"'createform'"'));"  type="submit" value="post">Confirm Proxy-Domain</button></td></tr>
</table>
    <table class=" border border-opacity-75 border-black rounded-md text-3xl">
     <tr class=" border border-opacity-75 border-black " ><td class=" border border-opacity-75 border-black" ><center><label for="target">Target (Real Domain):             </label>            </center></td><td class=" border border-opacity-75 border-black" ><center> <select class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " id=target name="target" placeholder="select container">'$(http_env_files_as_html_select)' </select></center></td><td></td></tr>
     <tr class=" border border-opacity-75 border-black " ><td class=" border border-opacity-75 border-black" ><center><label for="domain">Desired domain:                   </label>            </center></td><td class=" border border-opacity-75 border-black" ><center> <input style="width: 666px" type="text" id="domain" name="domain" onblur="DNSstatusToElement(this.value);SPFstatusToElement(this.value);validateDomain(this);" placeholder="mydomain.lan" autocomplete="off"></center> </td></tr>
     <tr class=" border border-opacity-75 border-black " ><td class=" border border-opacity-75 border-black" ><center><label for="composefiletype">Domain Type ( include www. )            </label>            </center></td><td class=" border border-opacity-75 border-black" ><center> <select class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " id=composefiletype name="composefiletype" placeholder="select container"> <option value="fulldomain"> full domain ( include www. )</option>  <option value="singledomain">Only the domain ( no www. )</option> </select></center></td><td></td></tr>
     <!-- <tr class=" border border-opacity-75 border-black " ><td class=" border border-opacity-75 border-black" ><center><label for="projectspace">Project/Customer: </label>         </center></td><td class=" border border-opacity-75 border-black" ><center> <select class="text-black ml-auto mr-auto flex-grow" id=projectspace name="projectspace">'$(_list_projects_htmloptions )' </select></center> </td></tr> -->

    <tr><td></td><td></td>
        <td class="bg-black text-white rounded-lg border border-opacity-75 border-black ">Check</td><td class="bg-white rounded-lg border border-opacity-75 border-black "> Result</td></tr>
    <tr><td></td><td></td>
        <td class="text-black bg-white rounded-lg border border-opacity-75 border-gray " >SPF</td><td class="bg-black rounded-lg border border-opacity-75 border-black "><code id=spfresult>Waiting</code><br></td></tr>
    <tr><td>.</td><td></td><td>.</td></tr>
    <tr><td></td><td></td>

        <td class="text-black bg-white rounded-lg border border-opacity-75 border-gray " >DNS</td><td class="bg-black rounded-lg border border-opacity-75 border-black "><code id=dnsresult>Waiting</code><br></td></tr>
    </table>
    </center>


    </div></form>
  <div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
   <div class=" border border-opacity-75 border-blue-500 bg-black bg-opacity-75 text-white"><h3 class="ml-auto rounded-md mr-auto self-center text-3xl">... 2nd step:change values</h3></div>
    <iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 666px;overflow:hidden;width: 90%" class="ml-auto flex-grow mr-auto self-center text-3xl" name="change-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe>
  </div>'

# <!--    <iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 666px;overflow:hidden;width: 90%" class="ml-auto flex-grow mr-auto self-center text-3xl" name="create-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe> -->
  echo '
  </body></html>'
  exit 0

fi

#### ENVIRONMENT CHANGES TEMPLATE
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=envchange' ]] ;then

echo "$html_header" "$javascript_validate_functions" '</head><body>
  <form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?template=realenvchange" target="change-iframe" method="post" autocomplete="off">
    <div class=" border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
    <div class=" border border-opacity-75 border-blue-500 bg-black bg-opacity-75 text-white flex rounded-md"> ... <h3><label for="target">1st step: select container</label></h3></div>
    <center>
        <select class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " id=target name="target" placeholder="select container">'$(env_files_as_html_select)' </select></center>
    <button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" type="submit" value="post">Change selected container</button>
    </div>
  </form>
  <div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
  <div class=" border border-opacity-75 border-blue-500 bg-black bg-opacity-75 text-white"><h3 class="ml-auto rounded-md mr-auto self-center text-3xl">... 2nd step:change values</h3></div>
    <iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 666px;overflow:hidden;width: 90%" class="ml-auto flex-grow mr-auto self-center text-3xl" name="change-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe>
  </div>
  </body></html>'
  exit 0
fi

#### ENVIRONMENT PRINT TEMPLATE
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=envprint' ]] ;then

echo "$html_header" "$javascript_validate_functions" '</head><body>
  <form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?action=printcontainerenv" target="printenv-iframe" method="post" autocomplete="off">
    <div class=" border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
    <div class=" border border-opacity-75 border-blue-500 bg-black bg-opacity-75 text-white flex rounded-md"> ... <h3><label for="target">1st step: select container</label></h3></div>
    <center>
        <select class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " id=target name="target" placeholder="select container">'$(env_files_as_html_select)' </select></center>
    <button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" type="submit" value="post">Show Container Backend Settings</button>
    </div>
  </form>
  <div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
  <div class=" border border-opacity-75 border-blue-500 bg-black bg-opacity-75 text-white"><h3 class="ml-auto rounded-md mr-auto self-center text-3xl">... 2.. get container values</h3></div>
    <iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 666px;overflow:hidden;width: 90%" class="ml-auto flex-grow mr-auto self-center text-3xl" name="printenv-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe>
  </div>
  </body></html>'
  exit 0
fi

#### MAINTAIN INSTANCE TEMPLATE
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=maintain' ]] ;then

echo "$html_header" "$javascript_validate_functions" '</head><body>
  <form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?action=maintain" target="change-iframe" method="post" autocomplete="off">
    <div class=" border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
    <div class=" border border-opacity-75 border-blue-500 bg-black text-white flex rounded-md"> ... <h3><label for="target">select container and action</label></h3></div>

    <center>
    <h1> Container </h1><select class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " id=target name="target" placeholder="select container">'$(env_files_as_html_select)' </select></center>
    <h1> Action: </h1> <select class="text-black focus:outline-none focus:shadow-outline border border-blue-300 rounded-lg py-2 px-4 block w-full appearance-none " id=targetaction name="targetaction" placeholder="select container">'$(targetactions_as_html_select)' </select></center>
    <button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" type="submit" value="post">Commit action</button>
    </div>
  </form>
  <div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
  <div class=" border border-opacity-75 border-blue-500 bg-black text-white">
     <h3 class="ml-auto rounded-md mr-auto self-center text-3xl">... 2nd step: execute action</h3></div>
    <iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 666px;overflow:hidden;width: 90%" class="ml-auto flex-grow mr-auto self-center text-3xl" name="change-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe>
  </div>
  </body></html>'
  exit 0
fi

##################################################

#### CREATE HOST template

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=preparecreate' ]] ;then

echo "$html_header" "$javascript_validate_functions" '</head><body class="bg-black text-white"> <meta http-equiv="refresh" content="6; URL=./_deploy.sh?template=create">
<center>
<div class=" border border-opacity-75 border-yellow-300 bg-black ml-auto  mr-auto self-center">
<table style="width: 100%;border: 0px"> <thead><tr style="border: 0px"><td>
<span class="  bg-black font-semibold text-r tracking-tight" ><img style="max-width: 200px;" class="roundedö-full ml-auto mr-auto self-center lg:items-center" src="/images/_load_rainbow_circle.gif"></span>
</td><td>
<center>  <span style="width: 50%" class=" text-yellow bg-opacity-50 bg-blue-400 border bg-blue-400  rounded-lg self-center text-3xl">Updating / Downloading presets  </span></center>
</td><td>
<span class="font-semibold text-r tracking-tight"><a href="/"><center> Docker-deployer <img class="ml-auto mr-auto self-center lg:items-center h-12 w-12" src="/images/dock66.svg"><center></a></span>
</td></tr></table>
</div>
</center>

  <div id=msgtxt class=" border border-opacity-75 border-blue-600 ml-auto flex-grow mr-auto self-center"> </div>'

    echo '<script>redirectURL = "/cgi-bin/_deploy.sh?template=create";urlPROTO="https://"
     setTimeout("location.href =  urlPROTO + document.location.host +redirectURL;",1000); </script>'
  echo '</body>'

### git pull all presets and output via js to "msgtxt" element
  _update_deployer_preset_library 2>&1   | while read line;do html_to_add=$(echo $line |sed 's/\(\\|r\)/<br>/g'); echo '<script> document.getElementById("msgtxt").innerHTML = document.getElementById("msgtxt").innerHTML + "'${html_to_add}'<hr>" ; </script>';done

fi

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=create' ]] ;then


echo "$html_header" "$javascript_validate_functions" '</head><body>
  <form id="createform" accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?template=verifycreate" containertype="create-iframe" method="post" autocomplete="off">
    <div class=" border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
    <div class=" border border-opacity-75 border-blue-500 bg-black text-white flex rounded-md"> ... <h3><label for="containertype">1st step: select container type ,project space and domain</label></h3></div>
    <center>
    <table class=" border border-opacity-75 border-black rounded-md text-3xl">
    <tr><td></td><td></td><td>Move your mouse <br>over the button<br> to verify the domain</td><td><button  class=" absolute top-10 right-10  flex-grow lg:flex  lg:w-auto bg-blue-600  hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" onMouseOver="DNSstatusToElement(document.getElementById('"'domain'"').value);SPFstatusToElement(document.getElementById('"'domain'"').value);validateDomain(document.getElementById('"'domain'"'));" onClick="hideElement(this);hideElement(document.getElementById('"'createform'"'));"  type="submit" value="post">Create container</button></td></tr>
</table>
    <table class=" border border-opacity-75 border-black rounded-md text-3xl">

     <tr class=" border border-opacity-75 border-black " ><td class=" border border-opacity-75 border-black" ><center><label for="containertype">Desired type: </label>            </center></td><td class=" border border-opacity-75 border-black" ><center> <select class="text-black ml-auto mr-auto flex-grow" id=containertype name="containertype">'$(_container_presets_available)' </select></center> </td><td></td></tr>
     <tr class=" border border-opacity-75 border-black " ><td class=" border border-opacity-75 border-black" ><center><label for="domain">Desired domain: </label>                 </center></td><td class=" border border-opacity-75 border-black" ><center> <input style="width: 666px" type="text" id="domain" name="domain" onblur="DNSstatusToElement(this.value);SPFstatusToElement(this.value);validateDomain(this);" placeholder="mydomain.lan" autocomplete="off"></center> </td></tr>
     <tr class=" border border-opacity-75 border-black " ><td class=" border border-opacity-75 border-black" ><center><label for="projectspace">Project/Customer: </label>         </center></td><td class=" border border-opacity-75 border-black" ><center> <select class="text-black ml-auto mr-auto flex-grow" id=projectspace name="projectspace">'$(_list_projects_htmloptions )' </select></center> </td></tr>

    <tr><td></td><td></td>
        <td class="bg-black text-white rounded-lg border border-opacity-75 border-black ">Check</td><td class="bg-white rounded-lg border border-opacity-75 border-black "> Result</td></tr>
    <tr><td></td><td></td>
        <td class="text-black bg-white rounded-lg border border-opacity-75 border-gray " >SPF</td><td class="bg-black rounded-lg border border-opacity-75 border-black "><code id=spfresult>Waiting</code><br></td></tr>
    <tr><td>.</td><td></td><td>.</td></tr>
    <tr><td></td><td></td>

        <td class="text-black bg-white rounded-lg border border-opacity-75 border-gray " >DNS</td><td class="bg-black rounded-lg border border-opacity-75 border-black "><code id=dnsresult>Waiting</code><br></td></tr>
    </table>
    </center>


    </div></form>
  <div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
  <div class=" border border-opacity-75 border-blue-500 bg-black text-white"><h3 class="ml-auto rounded-md mr-auto self-center text-3xl">... 2nd step:adjust values</h3></div>'
# <!--    <iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 666px;overflow:hidden;width: 90%" class="ml-auto flex-grow mr-auto self-center text-3xl" name="create-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe> -->
  echo '</div>
  </body></html>'
  exit 0
fi
## rest of creation functions after parsing post

### GET above , POST parsed below


## PARSE POST_STRING ###########################
saveIFS=$IFS
IFS='=&'
parm=($POST_STRING)
IFS=$saveIFS

declare -A param
for ((i=0; i<${#parm[@]}; i+=2)); do
#    param[${parm[i]}]=${parm[i+1]}; done
    ## Props: https://stackoverflow.com/a/3919908
    param[${parm[i]}]=$( _urldecode ${parm[i+1]} ); done

### END POST PARSER

### LIVE LOG
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?log=' ]] ;then
#tailfile=$(echo "$REQUEST_URI"|sed 's/\(?\|&\)/\n/g'|grep "log="|cut -d"=" -f2)
#taillines=$(echo "$REQUEST_URI"|sed 's/\(?\|&\)/\n/g'|grep "lines="|cut -d"=" -f2)
#tail -n ${taillines} /dev/shm/${tailfile}
tail -n $(echo "$REQUEST_URI"|sed 's/\(?\|&\)/\n/g'|grep "lines="|cut -d"=" -f2) /dev/shm/$(echo "$REQUEST_URI"|sed 's/\(?\|&\)/\n/g'|grep "log="|cut -d"=" -f2|sed 's/^\/dev\/shm//g')
fi
## LIVE LOG end



#####   template for proxy domain review
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=proxydomain_create' ]] ;then

echo "$html_header" "$javascript_validate_functions" "$js_live_love_log"'</head><body>'
[ "$DEBUGME" = "TRUE" ] && echo debug '<center><h1>Domain creation '$domain'</h1></center><br>'

checks_ok=yes;
domain="${param[domain]}" #
#projectspace="${param[projectspace]}";# ( composeroot subfolder )


## TODO single or muli
composefiletype="fulldomain"

#composefiletype="${param[composefile]}";# ( composeroot subfolder )
#[ -z "${composefiletype}" ] && composefile=docker-compose-multi.yml

giturl="$(cat /etc/docker_library.domainadd|head -n1|cut -f1)"
repofolder=/dev/shm/docker_container_lib/${giturl//*\//};

test -e ${repofolder} || git clone --recurse-submodules "${giturl}" "${repofolder}"

#targetenv="${COMPOSE_ROOT}/${param[target]}/.env"
sourceenv="${COMPOSE_ROOT}/${param[target]}"
## compose folder
sourcedir="${COMPOSE_ROOT}/${targetenv//\.env/}/"
## the "folder " above compose dir

projectspace=$(echo "${param[target]}"|sed 's/^\///g'|cut -d"/" -f1)

targetenv="${COMPOSE_ROOT}/${projectspace}/${param[domain]}/.env"
## compose folder
targetdir="${COMPOSE_ROOT}/${projectspace}/${param[domain]}/"
## the "folder " above compose dir

  targetfolder=${COMPOSE_ROOT}/${projectspace}/${domain}
  ## try to detect VIRTUAL_HOST and VIRTUAL_PROTO of sourcedir

  ## in theory you need only one host since redirect magic happens in htaccess or app
  ## one could try target_hostname=$(grep VIRTUAL_HOST ${sourceenv}|cut -d= -f2 |sed 's/.\+,//g' )
  ## but we ASSUME APP_URL (!!)
  source_hostname=$(grep APP_URL ${sourceenv}|cut -d"=" -f2)


  param[TARGET_URL]=${source_hostname}
##get the virtual host and port from container named APP_URL (might fail)
  sourceproto=$( docker exec -t "${source_hostname}" printenv |grep VIRTUAL_PROTO|cut -d= -f2);
  sourceport=$( docker exec -t "${source_hostname}" printenv |grep VIRTUAL_PORT |cut -d= -f2);

## check other similar containers named APP_URL_abcde  , set them as target_url if necessary
  for suffix in webserver nginx apache; do 
  [[ -z "$sourceproto" ]] &&  { tempval=$( docker exec -t "${source_hostname}_${suffix}" printenv |grep ^VIRTUAL_PROTO|cut -d= -f2) ; 
                                [[ -z "$tempval" ]] || { sourceproto=${tempval} ; param[TARGET_URL]=${source_hostname}_${suffix} ; } ; } ;

  [[ -z "$sourceport" ]]  &&  { tempval=$( docker exec -t "${source_hostname}_${suffix}" printenv |grep ^VIRTUAL_PORT |cut -d= -f2) ;
                                [[ -z "$tempval" ]] || { sourceport=${tempval} ; param[TARGET_URL]=${source_hostname}_${suffix} ; } ; } ;
  done
  #echo ${sourceproto} | grep -i -e ^http$  && proto_type=http
  #echo ${sourceproto} | grep -i -e ^https$ && proto_type=https

  #echo ${sourceproto} | grep -i -e ^http$  && param[TARGET_PORT]=80
  #echo ${sourceproto} | grep -i -e ^https$ && param[TARGET_PORT]=443
  
  ## TODO single or muli
  composefiletype=${param[composefiletype]}
  param[APP_URL]=${param[domain]}
  param[sourceport]=${sourceport}
  param[sourceproto]=${sourceproto}

  test -e ${COMPOSE_ROOT}/${projectspace}  || { _echo_redbox  "prxy NO PROJECTFOLDER:<br>CANNOT WRITE ON NONEXISTING FOLDER: ${COMPOSE_ROOT}/${projectspace} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## check if project  folder is  writable
  test -e ${COMPOSE_ROOT}/${projectspace} && [ ! -w "${COMPOSE_ROOT}/${projectspace}" ] && { _echo_redbox  "7xx NO WRITE PERMISSIONS FOR PROJECTFOLDER:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## project exists, do nothing
  test -e ${COMPOSE_ROOT}/${projectspace}/${domain} && [ ! -w "${COMPOSE_ROOT}/${projectspace}" ] && { _echo_redbox  "7xx ALREADY EXISTS:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  test -e ${COMPOSE_ROOT}/${projectspace}/${domain} && [ ! -w ${COMPOSE_ROOT}/${projectspace}}/${domain} ] &&  { _echo_redbox  "7xx NO PERMISSIONS BUT EXISTS:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  [ "$DEBUGME" = "TRUE" ] && echo "targetenv ${targetenv} composefile ${composefile} sourceproto ${sourceproto} sourceport ${sourceport} repofolder ${repofolder} targetdir ${targetdir}"
  [ "$DEBUGME" = "TRUE" ] && echo domain ${param[domain]}

  
  [[ "${composefiletype}" = "singledomain" ]] && { param[LETSENCRYPT_HOST]=${param[APP_URL]} ; param[VIRTUAL_HOST]=${param[APP_URL]} ; } ;
  [[ "${composefiletype}" = "fulldomain" ]]   && { param[LETSENCRYPT_HOST]=www.${param[APP_URL]},${param[APP_URL]} ; param[VIRTUAL_HOST]=www.${param[APP_URL]},${param[APP_URL]} ; } ;
  
  for neededparm in composefiletype sourceport sourceproto APP_URL LETSENCRYPT_HOST VIRTUAL_HOST TARGET_URL; do 
    [[ -z "${param[$neededparm]}" ]] && _echo_redbox "PARAMETER MISSING:$neededparm"
  done
  [[ "$checks_ok" = "yes" ]] &&  param[TARGET_PORT]=$(echo "$sourceport"  | sed 's/ //g;s/\r//g;s/\t//g;')
  [[ "$checks_ok" = "yes" ]] && param[TARGET_PROTO]=$(echo "$sourceproto" | sed 's/ //g;s/\r//g;s/\t//g;')
  
  
  echo ${sourceproto} | grep -i -e ^http$  && param[TARGET_PROTO]=https
  echo ${sourceproto} | grep -i -e ^https$ && param[TARGET_PROTO]=https

  [ -z "$sourceproto" ]  && { _echo_redbox  "prxy COULD NOT DETECT sourceproto FROM port $sourceport ";checks_ok=no ;  } ;
  [ -z "$sourceport" ]   && { _echo_redbox  "prxy COULD NOT DETECT sourceproto FROM port $sourceport ";checks_ok=no ;  } ;

  if
   [ "$checks_ok" = "yes" ];then
   _echo_blueb "detected port and proto "$sourceport" "$sourceproto"<br>"
  echo '<div id=reviewdeploy  class=" rounded-lg border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">'
_echo_bluebox "Check values finally"
  echo '<form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?action=bootstrap_proxydomain" target="compose-iframe" method="post" autocomplete="off">
      <center><table  class=" border border-opacity-75 border-blue-500 bg-blue-200 ml-auto flex-grow mr-auto self-center text-xl" >
      <thead> <tr><td><center>'$(_echo_bluebutton Setting)'</center></td><td><center>'$(_echo_redbutton Value)'</center></td><td>'$(_echo_bluebox Hint)'</td> </thead>'

echo '<tr><td class=" text-white border border-opacity-75 border-blue-500"><center>'$(_echo_bluebox_m domain)'</center></td><td class=" border border-opacity-75 border-blue-500">'${domain}'</td></td><td class=" border border-opacity-75 border-blue-500">the chosen domain</td></tr>' ;
echo '<tr><td class=" text-white border border-opacity-75 border-blue-500"><center>'$(_echo_bluebox_m projectspace)'</center></td><td class=" border border-opacity-75 border-blue-500">'${projectspace}'</td></tr>';
envraw=$(cat ${repofolder}/DOTenv.example|grep -v '^#')
envindexes=$(echo "$envraw"|grep -v '^#'|cut -d= -f1)
#indexes="${!param[@]}"


## FILL PARAM WITH MISSING ENV
#for curindex in $envindexes; do
#[ ${param[$curindex]+abc} ] || { param[$curindex]=$(echo "$envraw"|grep "^${curindex}"|cut -d= -f2-) ; [ "$DEBUGME" = "TRUE" ] && echo "<tr><td>debug filled $curindex , is now "${param[$curindex]}"</td></tr>"  ; } ;
#done


for index in "${!param[@]}"; do
    if [ ! -z "${param[$index]}" ] ; then
    #echo "${param[$index]}" |grep -q ^[A-Z] && echo "<tr><td>"${index}'</td><td>'${param[$index]}'</td>' ;
    echo -n '<tr><td class=" text-white border border-opacity-75 border-blue-500"><center>'$(_echo_bluebox_m ${index})'</center></td><td class=" border border-opacity-75 border-blue-500">'${param[$index]}'</td>' ;
    ##values will be sent 1:1 to next form
    echo -n '<td><input type="hidden" id="'$index'" name="'$index'" value="'${param[$index]}'"></td>'
    echo -n '</tr>';
    echo ;
  fi
done |sort -u

##echo '<input type="hidden" id="target" name="target" value="'${targetname}'">'
 #echo $POST_STRING
 logtarget="deploy-docker-proxy."$(date +%s.%N)".log"

   echo '<tr><td ><input type="hidden" id="logtarget" name="logtarget" value="'$logtarget'"> </td> </tr>'


     echo '<tr><td></td><td><button id=deploybutton class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" onclick="trigger_logtail(' "'"${logtarget}"'" ');document.getElementById('"'"reviewdeploy"'"').style.display = '"'"none"'"' ;document.getElementById('"'"reviewdeploy"'"').style.height = '"'"30px"'"' ;" type="submit" value="post">Deploy container</button></td><td><div id="jschecktxt"></div></tr>'
echo '</table></center></form></div>'




          echo '<div id="actiondiv" class="  table-fixed rounded-lg bg-blue-500 bg-opacity-25 " > <center><table  style="width: 99%" class="  table-fixed rounded-lg bg-blue-400 bg-opacity-25 ml-auto flex-grow mr-auto self-center text-3xl" ><thead><tr><th class="w-3/4 px-4 py-2"><h2 style="align: right">Console Log: </h2></center></th> <th  class="text-sm w-1/2 px-4 py-2"><div style="max-width: 333px" id="idlestatus"  class="blink-text rounded-full self-center text-center bg-opacity-25 text-black border-blue-200 bg-gray-500"> Status: Waiting...</div></th></tr></thead></table></center>
          <center>
          <div style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 423px;overflow:hidden;width: 90%" class=" rounded-lg border border-opacity-75 border-blue-600 bg-black ml-auto flex-grow mr-auto self-center">
          <div style="width: 95%" id="msgtxt" class=" rounded-lg border border-opacity-75 text-left text-white border-yellow-600 bg-gray-900 ml-auto flex-grow mr-auto self-center text-sm"></div></div><hr></center>
          <div class=" rounded-lg border border-opacity-75 border-blue-600 bg-blue-500 ml-auto flex-grow mr-auto self-center text-3xl"><br>
          <center><iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 523px;overflow:hidden;width: 90%" class="ml-auto rounded-lg flex-grow mr-auto self-center text-white border-blue-600 bg-gray-400 " name="compose-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe></center>
          </div></div>'

else
  _echo_redbox "pre-checks failed"
fi
echo '</body></html>'
echo -n;
fi


######
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=realenvchange' ]] ;then
## this one has GET and POST
targetenv=$(cat /dev/shm/docker-envlist |grep "/${param[target]}/.env")
targetname=${targetenv%\/.env}
targetname=${targetname#\/}
#customer=

echo "$html_header" "$javascript_validate_functions" '

</head><body>
<div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
   <center><h3> Enter values for '"${targetname}"'</h3><div class="text-xl">(Passwords must have small & large letters , numbers and special characters ( only [._-+] allowed ) )</div></center>
<form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?template=submitenvchange" target="_blank" method="post" autocomplete="off">'

echo '<br><center><table class="table-auto  border-blue-300 bg-blue-100 ml-auto flex-grow mr-auto self-center text-2xl" style="min-width: 666px"><thead ><td class=" rounded-full border-black bg-blue-300">SETTING</td><td style="min-width: 555px" class=" rounded-full border-black bg-red-300">VALUE</td></thead><tbody>'

## ENV PARSER
realifs=$IFS;IFS=$'\n' ;
for entry in $(cat ${COMPOSE_ROOT}/${targetname}/.env |grep -v -e "^#" -e ^$) ; do

entrkey=${entry%%=*}
entrval=${entry#*=}
#[ "$DEBUGME" = "TRUE" ] && echo debug $entry "|" $entrykey "|" $entryval
case ${entrkey} in
    LETSENCRYPT_EMAIL )
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" type="email" pattern=".+@+.+" onblur="validateEmail(this);" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

    APT_HTTP_PROXY_URL )
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

    APP_APTPROXY|ENABLE_WWW_SHELL)
    if [ "${entrval}" = "true" ];then
      opposite=false
    else
      opposite=true
    fi
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td><select class="text-black" id="'${entrkey}'" name="'${entrkey}'" > <option value="'${entrval}'" selected>'${entrval}'(unchanged)</option><option value="'${opposite}'">'${opposite}'</option></select></td></tr>'
    echo -n " ";;

    MAIL_ADMINISTRATOR )
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" type="email" pattern=".+@+.+" onblur="validateEmail(this);" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

    *_PASSWORD )
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entry//?/.}'" autocomplete="off"></td></tr>'
    echo -n " ";;

    MAIL_DRIVER )
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td>
               <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="smtp" autocomplete="off"></td></tr>'
    #echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

    MAIL_* )
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td>
               <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

    CACHE_DRIVER) #https://stackoverflow.com/questions/45677563/laravel-which-cache-driver-to-use
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td><select class="text-black" id="'${entrkey}'" name="'${entrkey}'" > <option value="redis">redis (default integrated)</option><option value="database">database</option><option value="memcached">memcached</option><option value="file">file(Fallback)</option><option value="array">array (Debug)</option></select></td></tr>'
    echo -n " ";;

    SESSION_DRIVER) #https://stackoverflow.com/questions/17070851/laravel-session-drivers
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td>
               <td><select class="text-black" id="'${entrkey}'" name="'${entrkey}'" ><option value="redis" selected>redis (default)</option><option value="memcached">memcached</option> <option value="cookie">cookie</option><option value="database">database</option><option value="native">native PHP (Fallback)</option><option value="array">array(DEBUG, non-persistent)</option></select></td></tr>'
    echo -n " ";;

    QUEUE_DRIVER)  #preferring database over sync over redis https://laracasts.com/discuss/channels/laravel/which-queue-driver-is-faster-database-or-redis
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td>
               <td><select class="text-black" id="'${entrkey}'" name="'${entrkey}'" > <option value="database">database (default)</option> <option value="sync">sync</option> <option value="redis">redis</option></select></td></tr>'
    echo -n " ";;

    COMPOSE_PROJECT_NAME )
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td>
               <td><select id="'${entrkey}'" name="'${entrkey}'" >
                    <option value="'${entrval}'">'${entrval}'(unchanged)</option>
                    <option value="'${targetname%%\/*}'">'${targetname%%\/*}' (customer name)</option>
                    <option value="'$(echo ${targetname//*\/}|sed 's/\.//g')'">'$(echo ${targetname//*\/}|sed 's/\.//g')' (site name)</option></select></td></tr>'
    echo -n " ";;


####### greyed out

    *_PORT )
      echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td>
                 <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;


###### hidden options ( since user shall not screw it up)
    MARIADB_* ) echo -n " ";;
    APP_URL ) echo -n " ";;
    HOSTNAMES ) echo -n " ";;
    SSL_POLICY ) echo -n " ";;
    STORAGE_ROOT ) echo -n " ";;
  #  *_PORT ) echo -n " ";;
    INSTALL_* ) echo -n " ";;
    VIRTUAL_P* ) echo -n " ";;
    LETSENCRYPT_HOST ) echo -n " ";;

    *)
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

esac
done;IFS="$realifs"
echo '<tr><button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" onClick="hideElement(this);" type="submit"  value="post">Review and confirm changes</button></center></tr>'
### now the target needs to be set as well

echo '</tbody></table>'
echo '<input type="hidden" id="target" name="target" value="'${targetname}'">'
echo '</form></div></body></html>'
exit 0
fi

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=submitenvchange' ]] ;then
  targetenv=$(cat /dev/shm/docker-envlist |grep "/${param[target]}/.env"|grep $param[projectname] |head -n1)
  targetname=${targetenv%\/.env}
  targetname=${targetname#\/}
  echo ${html_header} "${js_live_love_log}" '
</head><body>
<div class="text-center border border-blue-500 rounded bg-blue-500 hover:bg-blue-700 text-white block lg:flex lg:items-center " >
<span class="font-semibold text-l tracking-tight"><a href="/"> Docker-deployer <img class="ml-auto mr-auto self-center lg:items-center h-12 w-12" src="/images/dock66.svg"></a></span> </div>
  <div class=" border border-opacity-75 border-blue-600 bg-black text-yelllow ml-auto flex-grow mr-auto self-center text-3xl"><center><h3> Verifying Changes for '"${targetname}"'</h3></center>'

[ ! -w "${COMPOSE_ROOT}${targetenv}" ] && { _echo_redbox  "NO WRITE PERMISSIONS:<br>CANNOT WRITE ON ${COMPOSE_ROOT}${targetenv} as $(id -un) ($(id -u)) ";echo "</body></html>" ; exit 23; }

echo '<form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?template=triggerenvchange" target="compose-iframe" method="post" autocomplete="off">'
echo '<input type="hidden" id="target" name="target" value="'${targetname}'">'
echo '<div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl"><center>
<table class="table-auto  border-blue-300 bg-blue-100 ml-auto flex-grow mr-auto self-center text-2xl" style="min-width: 666px"><thead ><tr><td class=" rounded-full border-black bg-blue-300">ORIGINAL</td><td  class=" rounded-full border-black bg-red-300">CHANGED</td></tr></thead><tbody> '
for index in "${!param[@]}"; do
  if [ ! "${index}" = "target" ];then
    if [ ! -z "${param[$index]}" ] ; then
      #echo search "${COMPOSE_ROOT}${targetenv}"
      #newset="${index}=${param[$index]}"
      #echo $origset $newset
      #  echo "$index ${param[$index]} <br>";
      origset=$(grep "$index" "${COMPOSE_ROOT}${targetenv}" )
      if [ ! "${origset}" = "${index}=${param[$index]}" ];then
        if [[ ${origset} =~ "PASSWORD" ]] ;then
          passverify=`echo ${param[$index]} | egrep "^.{12,255}" | egrep "[ABCDEFGHIJKLMNOPQRSTUVWXYZ]" | \
              egrep "[abcdefghijklmnopqrstuvwxyz"] | egrep "[0-9]"| \
              grep -v -e ABC -e BCD -e CDE -e DEF -e EFG -e FGH -e GHI -e HIJ -e IJK -e JKL -e KLM -e LMN -e MNO -e NOP -e OPQ -e PQR -e QRS -e RST -e STU -e TUV -e UVW -e VWX -e WXY -e XYZ -e abc -e bcd -e cde -e def -e efg -e fgh -e ghi -e hij -e ijk -e jkl -e klm -e lmn -e mno -e nop -e opq -e pqr -e qrs -e rst -e stu -e tuv -e uvw -e vwx -e wxy -e xyz |egrep "[_.-\+]"`
          if [ -z "$passverify" ];then
          echo '<tr><td>'${index}'</td><td> UNCHANGED: PASSWORD TOO WEAK</td></tr>'
              else
                echo '<tr><td> '${index}'=OLDPASSWORD(MASKED) </td><td> '${index}'=NEWPASSWORD(MASKED)</td></tr>'
                echo '<input type="hidden" id="'${index}'" name="'${index}'" value="'${param[$index]}'"> </td> </tr>'
          fi
        else
            echo "<tr><td> ${origset} </td><td> ${index}=${param[$index]}  "
            echo '<input type="hidden" id="'${index}'" name="'${index}'" value="'${param[$index]}'"> </td> </tr>'
        fi
      fi;
    fi;
  fi
done
echo '</tbody></table></center></div>'
logtarget="deploy-docker."$(date +%s.%N)".log"


echo '<br>
  <button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full"  onclick="trigger_logtail(' "'"${logtarget}"'"');this.style.display='"'none'"' " type="submit" value="post">I AM SURE to commit the shown changes</button> '
  echo '<input type="hidden" id="logtarget" name="logtarget" value="'$logtarget'"> </td> </tr>'
echo '</form>'

echo '<center><table  style="width: 99%" class="  table-fixed rounded-lg bg-grey-500 ml-auto flex-grow mr-auto self-center text-3xl" ><thead><tr><th class="w-3/4 px-4 py-2"><h2 style="align: right">Console Log: </h2></center></th> <th  class="text-sm w-1/2 px-4 py-2"><div id="idlestatus"  class="blink-text rounded-full self-center text-center text-gray-100 border-blue-200 bg-gray-500"> Status: Waiting...</div></th></tr></thead></table>
<center>
<div style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 423px;overflow:hidden;width: 90%" class=" rounded-lg border border-opacity-75 border-blue-600 bg-black ml-auto flex-grow mr-auto self-center">
<div style="width: 95%" id="msgtxt" class=" rounded-lg border border-opacity-75 text-left text-white border-yellow-600 bg-gray-900 ml-auto flex-grow mr-auto self-center text-sm"></div></div><hr></center>
<div class=" rounded-lg border border-opacity-75 border-blue-600 bg-blue-500 ml-auto flex-grow mr-auto self-center text-3xl"><br><center><iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 235px;overflow:hidden;width: 90%" class="ml-auto rounded-lg flex-grow mr-auto self-center text-white border-blue-600 bg-gray-400 " name="compose-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe></center>'
#echo $POST_STRING
echo '</div></body></html>'
exit 0
fi

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=triggerenvchange' ]] ;then
## this one has GET and POST


targetenv=$(cat /dev/shm/docker-envlist |grep "/${param[target]}/.env")
targetname=${targetenv%\/.env}
targetname=${targetname#\/}
targetdir=$(dirname "${COMPOSE_ROOT}${targetenv}")
echo $html_header'</head><body>
<div class=" border border-opacity-75 text-white border-blue-600 bg-gray-500 ml-auto flex-grow mr-auto self-center text-3xl">'
[ ! -w "${COMPOSE_ROOT}${targetenv}" ] && { _echo_redbox  "NO WRITE PERMISSIONS:<br>CANNOT WRITE ON ${COMPOSE_ROOT}${targetenv} as $(id -un) ($(id -u)) ";echo "</body></html>" ; exit 23; }

echo "<center>logging to "${param[logtarget]}"</center>"

## begin output slope
 (
 echo  "################### ^ STARTING ^ ###################" |yellow
      uptime | yellow
    echo_blue "#### BACKING UP .env TO "'$COMPOSE_ROOT/'${targetdir#$COMPOSE_ROOT}/backup.env
    cp ${COMPOSE_ROOT}${targetenv} ${targetdir}/backup.env


    ### if "COMPOSE_PROJECT_NAME" was changed , we have to shutdown before
    docker ps -a  --format '{{.Names}}' |grep ^$(basename $(pwd))|xargs docker rm
    echo ${POST_STRING}|grep -v "COMPOSE_PROJECT_NAME=&" |grep -q COMPOSE_PROJECT_NAME &&  {
    _compose_down "${targetdir}" 2>&1
    cd ${targetdir}/ && docker ps -a  --format '{{.Names}}' |grep ^$(basename $(pwd))|xargs docker rm
    #    cd ${targetdir}/;docker container prune --filter $(basename $(pwd))
    echo -n ; } ;

    for index in "${!param[@]}"; do
       if [[ ! "${index}" =~ "target" ]] ;then
      echo " | replace $index "|yellow;echo
      sed 's/^'${index}'=.\+/'${index}'='${param[$index]}'/g' -i ${COMPOSE_ROOT}/${targetenv}
    fi
    done

    cd ${targetdir}


    echo "################### + DEPLOYING + ###################"| yellow
    echo ${POST_STRING}|grep -q "ENABLE_WWW_SHELL=" -q &&  {
      echo updating shell for www-data
      cd ${targetdir} ;
      echo enabling shell if necesaary
    cat "${COMPOSE_ROOT}/${targetenv}" |grep "ENABLE_WWW_SHELL=" |grep -e "=true" -q  && docker exec -t $(basename $(pwd)) chsh -s /bin/bash www-data
      echo disabling shell if necessary
    cat "${COMPOSE_ROOT}/${targetenv}" |grep "ENABLE_WWW_SHELL=" |grep -e "=false" -q && docker exec -t $(basename $(pwd)) chsh -s /usr/sbin/nologin www-data
    }
    echo ${POST_STRING}|grep -q "ENABLE_WWW_SHELL=" ||  _compose_up "${targetdir}"  2>&1
    echo "################### OPERATION FINISHED #########"|green

) |sed 's/\r/ /g'| _timestamper  | aha --no-header  |tee  /dev/shm/${param[logtarget]} 1>&2

## end output slope


#| while read -r line; do  strip_escape_codes "${line}" line_stripped;   echo "${line_stripped}" ;done | _timestamper  |tee  /dev/shm/${param[logtarget]} 1>&2
echo '<center><h3>Finished applying changes '"</h3></center>"
#echo $POST_STRING
echo '</div></body></html>'

fi

############## END ENV CHANGE




###### PRINT ENV


if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=printcontainerenv' ]] ;then
## this one has GET and POST
targetenv=$(cat /dev/shm/docker-envlist |grep "/${param[target]}/.env")
targetname=${targetenv%\/.env}
targetname=${targetname#\/}
#customer=

echo "$html_header" "$javascript_validate_functions" '

</head><body>
<div class=" border border-opacity-75 border-blue-600 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
   <center><h3> Values for '"${targetname}"'</h3><div class="text-xl">please be careful handing out this credentials</div></center>
<form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?action=donothing" target="_blank" method="post" autocomplete="off">'

echo '<br><center><table class="table-auto  border-blue-300 bg-blue-100 ml-auto flex-grow mr-auto self-center text-2xl" style="min-width: 666px"><thead ><td class=" rounded-full border-black bg-blue-300">SETTING</td><td style="min-width: 555px" class=" rounded-full border-black bg-red-300">VALUE</td></thead><tbody>'

## ENV PARSER
realifs=$IFS;IFS=$'\n' ;
for entry in $(cat ${COMPOSE_ROOT}/${targetname}/.env |grep -v -e "^#" -e ^$ | grep -e APP_URL -e DATABASE -e WORDPRESS_DB -e WP_DB -e MYSQL -e MARIADB -e TOKEN -e SSH_PORT -e PHP_VERSION |grep -v ROOT_PASSW|sort) ; do

entrkey=${entry%%=*}
entrval=${entry#*=}
#[ "$DEBUGME" = "TRUE" ] && echo debug $entry "|" $entrykey "|" $entryval
case ${entrkey} in

####### greyed out

    *_PORT )
      echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td>
                 <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;


###### hidden options ( since user shall not screw it up)
  #  MARIADB_* ) echo -n " ";;
   # APP_URL ) echo -n " ";;
    HOSTNAMES ) echo -n " ";;
    SSL_POLICY ) echo -n " ";;
    STORAGE_ROOT ) echo -n " ";;
  #  *_PORT ) echo -n " ";;
    INSTALL_* ) echo -n " ";;
    VIRTUAL_P* ) echo -n " ";;
    LETSENCRYPT_HOST ) echo -n " ";;

    *)
    echo '<tr><td><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" value="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

esac
done;IFS="$realifs"
echo '</tbody></table>'
echo '</form></div></body></html>'
exit 0
fi






#### list customers
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=listprojects' ]] ;then
echo $html_header'</head><body>'
echo '<div class="lg:w-auto bg-grey-400 ">'

find ${COMPOSE_ROOT}/ -mindepth 1 -maxdepth 1 -type d| sed 's/.\+\///g;'| sort |while read curproj;do
  echo '<div class="border-black border rounded=full" >'
  echo '<button class="bg-indigo-400 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-lg border border-grey text-3xl">';echo ${curproj}'</button><br><div style="width: 80%;margin-left: 23px" class="border-grey border rounded=full" >';
      for instance  in $(find  ${COMPOSE_ROOT}/${curproj} -mindepth 1 -maxdepth 1 -type d| sed 's/.\+\///g;' );do  echo '<button class="text-2xl border border-grey bg-black hover:bg-blue-700 text-white hover:text-white font-bold py-2 px-4 rounded-lg">';echo ${instance}'</button>';
     done
  echo '</div></div><hr>'
done
echo "</div></body>"
fi

#### adding customer/project
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=addproject' ]] ;then
    echo $html_header'<script>
      redirectURL = "/add_project_folder.html";urlPROTO="https://"
      setTimeout("location.href = urlPROTO + document.location.host +redirectURL;",1776);
      </script>'
     echo '<meta http-equiv="refresh" content="2; URL=./../add_project_folder.html">'
      echo '</head><body>'

    if [ -z "${param[project]}" ]; then
      _echo_redbox "empty projectname given"
    else
      clean_name=$(echo "${param[project]}" | tr -dc '0-9a-zA-z.' )
      _echo_bluebox Creating folder for  "${clean_name}"
      test -d /opt/docker-compose/webprojects/${clean_name} && _echo_redbox already exists
      test -d "${COMPOSE_ROOT}/${clean_name}" || mkdir "${COMPOSE_ROOT}/${clean_name}" && _echo_bluebox created "${COMPOSE_ROOT}/${clean_name}"  || _echo_redbox failed on  "${COMPOSE_ROOT}/${clean_name}"  2>&1
    fi
    echo '</body></html>'
fi

## creation step 2
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?template=verifycreate' ]] ;then
  checks_ok=yes;
##incoming params:
containertype="${param[containertype]}" # ( git repo -> check against reg)
domain="${param[domain]}" #
projectspace="${param[projectspace]}";# ( composeroot subfolder )
echo "$html_header" "$javascript_validate_functions" '</head><body>'
[ "$DEBUGME" = "TRUE" ] && echo debug '<center><h1>Domain creation '$domain'</h1></center><br>'

giturl=${containertype};
repofolder=/dev/shm/docker_container_lib/${giturl//*\//};
[ "$DEBUGME" = "TRUE" ] && echo debug repofolder ${repofolder} $(ls -1 ${repofolder}/docker-compose.yml ${repofolder}/docker-compose-single.yml ${repofolder}/docker-compose-multi.yml ${repofolder}/DOTenv.example 2>/dev/null)
ls -1 ${repofolder}/docker-compose.yml ${repofolder}/docker-compose-single.yml ${repofolder}/docker-compose-multi.yml 2>/dev/null  |grep "docker-compose"|grep yml -q && test -f ${repofolder}/DOTenv.example && { _echo_bluebutton "template is valid" ; }  || { _echo_redbox "Template invalid" ;checks_ok=no  ; } ;


spf_count=$(_nslookup "$domain" TXT|grep "v=spf"|wc -l);
## validate our spf records
[   "$spf_count" = 1 ] && _echo_bluebutton "SPF entry exists"
[ ! "$spf_count" = 1 ] && _echo_redbox "no SPF entry, you may be classified as spam when sending mail "
chosenport=0;
allusedports="P"$(_docker_get_all_ports)"P";
allusedports=$(echo $allusedports|sed 's/\( \|\t\|^\|$\)/P/g') ;
for testport in $(seq 22000 29999);do
    if [[ ! "${allusedports}" =~ P${testport}$P ]]; then chosenport=$testport ;break;fi;
done;
random_ssh_port=$chosenport
if [ -z "$random_ssh_port" ] ;then echo_redbox "could not find ssh port" ;checks_ok=no ;else _echo_bluebutton "ssh port used: $random_ssh_port";fi
[ "$DEBUGME" = "TRUE" ] && echo "port $random_ssh_port OF allused ${allusedports}"
single_template=no;
[ "$DEBUGME" = "TRUE" ] && echo debug "nxdomain"
host  "$domain" &>/dev/null  && { _echo_bluebutton "domain resolves "
      echo "$domain" |grep -q "^www\." &&  { _echo_redbox "Domain shall not start with www." ; checks_ok=no ; } ;
      echo "$domain" |grep -q "^www\." ||  { echo "$domain" |grep -q "\." && _echo_bluebutton "domain seems OK" ; } ;
      host  "www."$domain &>/dev/null &&  { _echo_bluebutton "<b>www.</b>domain resolves"  ; } || { _echo_redbox "<b>www.</b>.$domain does not resolve" ; }  ;

  } || { _echo_redbox "could not lookup domain: "$(host  "$domain") ; checks_ok=no ; }

  ## find existing conainers
  docker ps -a --format '{{.Names}}'|grep ^${domain}$ -q && { _echo_redbox "container named  <code> ${domain} </code> does ALREADY EXIST " ; checks_ok=no ; } ;
  docker ps -a --format '{{.Names}}'|grep ^${domain}$ -q || { _echo_bluebutton "container with name $domain does not exist" ; }

  ## find existing conainers

  find ${COMPOSE_ROOT} -maxdepth 2 -mindepth 2|sed 's/.\+\///g'|grep ^${domain}$ -q && { _echo_redbox "container named  <code> ${domain} </code> is ALREADY DEFINED HERE : " $(  find ${COMPOSE_ROOT} -maxdepth 2 -mindepth 2 |grep "/"${domain}$ |sed 's/'${COMPOSE_ROOT//\//.}'//g' ) "   ) .." ; checks_ok=no ; } ;
  find ${COMPOSE_ROOT} -maxdepth 2 -mindepth 2|sed 's/.\+\///g'|grep ^${domain}$ -q || { _echo_bluebutton "container with name $domain is not defined already" ; }




[ "$DEBUGME" = "TRUE" ] && echo debug "$myv4" $(grep -q "$myv4" /realproc/net/fib_trie)

## find ipv4
for myv4 in $(_nslookup_ip4 $domain);do
  grep -q "$myv4" /realproc/net/fib_trie && _echo_bluebutton "IPv4 $myv4 of $domain points to us " ;
  grep -q "$myv4" /realproc/net/fib_trie || { _echo_redbox "IPv4 $myv4 of $domain does NOT point to this machine " ; checks_ok=no ; } ;done
#find ipv6
publicv6=$(for i in "$(grep /realproc/net/if_inet6 -v -e lo$ -e ^fe80  )"; do     echo "$i" | gawk '@include "join"
  {
      split($1, _, "[0-9a-f]{,4}", seps)
      print join(seps, 1, length(seps), ":")
  }'; done |sed 's/:0\+/:/g')

for myv6 in $(_nslookup_ip6 $domain|grep ":"|sed 's/:0\+/:/g');do
     [ "$DEBUGME" = "TRUE" ] && echo debug search ip "${myv6//::/:}"
     echo "${publicv6//:*/:}"|grep -q ^"${myv6//:*/:}"  && { dnsmsg=${dnsmsg}" | IPv6 $myv6 of $domain points to us " ; } 
     echo "${publicv6//:*/:}"|grep -q ^"${myv6//:*/:}"  || { dnsmsg=${dnsmsg}'\n'" | IPv6 $myv6 of $domain does NOT point to this machine ( PUB: ${publicv6//::*/::} ME: ${myv6//::*/::} ) " ; checks_ok=no ; } ;
   done
[ "$DEBUGME" = "TRUE" ] && echo debug publicv6 $publicv6


_randpass() { for rounds in $(seq 1 32);do cat /dev/urandom |tr -cd '[:alnum:]_\-.'  |head -c18;echo ;done|grep -e "_" -e "\-" -e "\."|grep ^[a-zA-Z0-9]|grep [a-zA-Z0-9]$|tail -n1 ;  } ;
if [ "$checks_ok" = "yes" ];then
echo '<form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?action=deploy" containertype="create-iframe" method="post" autocomplete="off">
    <div class=" border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">
    <center><table class=" border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-2xl" >
    <thead> <tr><td>Setting</td><td>Value</td><td>Hint</td> </thead>'

##### ENV PARSING START
realifs=$IFS;IFS=$'\n' ;
for entry in $(cat ${repofolder}/DOTenv.example |grep -v -e "^#" -e ^$) ; do

entrkey=${entry%%=*}
entrval=${entry#*=}
#[ "$DEBUGME" = "TRUE" ] && echo debug $entry "|" $entrykey "|" $entryval
case ${entrkey} in
      APP_URL )
      echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="ro'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="ro-domain" name="domain" placeholder="'${domain}'" autocomplete="off"></td></tr>'
      echo -n " ";;

      *_DATABASE|WORDPRESS_DB_NAME )
      preset=$(echo ${entrval}"-"${domain//./} |head -c 16)
      echo '<tr class="border border-opacity-75 border-blue rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%"  type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${preset}'" value="'${preset}'" autocomplete="off"></td></tr>'
      echo -n " ";;

      WORDPRESS_ADMIN_USER )
      #preset=${domain//./}
      preset=WPadm-$(cat /dev/urandom |tr -cd '[:alnum:]'|head -c5)"-"${domain//.*/}
      echo '<tr class="border border-opacity-75 border-blue rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" value="'${preset}'"  autocomplete="off"></td>'
      echo '<td class="border border-opacity-75 border-black rounded-lg"> Backend .htaccess providing full access </td>'
      echo '</tr>'
      echo -n " ";;

      TOKEN_USER )
      #preset=${domain//./}
      preset=adm-$(cat /dev/urandom |tr -cd '[:alnum:]'|head -c5)"-"${domain//.*/}
      echo '<tr class="border border-opacity-75 border-blue rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" value="'${preset}'"  autocomplete="off"></td>'
      echo '<td class="border border-opacity-75 border-black rounded-lg"> Backend .htaccess providing full access </td>'
      echo '</tr>'
      echo -n " ";;

      MARIADB_USERNAME|MYSQL_USERNAME|WORDPRESS_DB_USER )
      #preset=${domain//./}
      preset=$(echo ${entrval}"-"${domain//./} |head -c 16)
      echo '<tr class="border border-opacity-75 border-blue rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" value="'${preset}'"  autocomplete="off"></td></tr>'
      echo -n " ";;

      TOKEN_GUEST_PASS )
      #preset=${domain//./}
      preset=${entrval}"-"${domain//./}-$(_randpass)
      [ -z "${entrval}" ] &&  preset=""
      [ "${entrval}" = "NoPassword" ] &&  preset="NoPassword"
      echo '<tr class="border border-opacity-75 border-blue rounded-lg" > <td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" value="'${preset}'"  autocomplete="off"></td>'
      echo '<td class="border border-opacity-75 border-black rounded-lg"> Guests Password  .htaccess on /login url (name:users) ,<br> use  <br><br> <code>NoPassword</code><br>   or leave empty for open /login urls</td>'
      echo '</tr>'
      echo -n " ";;

      MARIADB_PASSWORD|MYSQL_PASSWORD|WORDPRESS_DB_PASSWORD|TOKEN_PASS|WORDPRESS_ADMIN_PASS|MAIL_PASSWORD )

      if [ "${entrkey}" = "MAIL_PASSWORD" ] ; then
        preset="...........";
      else
        preset=$(_randpass)
      fi
      echo '<tr class="border border-opacity-75 border-red rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'"  value="'${preset}'" placeholder="'${preset}'" autocomplete="off"></td>'
      [ "${entrkey}" = "TOKEN_PASS" ] && echo '<td class="border border-opacity-75 border-black rounded-lg"> passphrase for .htaccess backend user  </td>'
      echo '</tr>'
      echo -n " ";;

      MARIADB_ROOT_PASSWORD|MYSQL_ROOT_PASSWORD )
      preset=$(_randpass)$(cat /dev/urandom |tr -cd '[:alnum:]'|head -c5)
      echo '<tr class="border border-opacity-75 border-red rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="ro-'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="ro-'${entrkey}'" name="ro-'${entrkey}'" value="'${preset}'" placeholder="'${preset}'" autocomplete="off"></td>'
      #disabled so we need hidden for submission
      echo '<td><input type="hidden" id="'${entrkey}'" name="'${entrkey}'" value="'${preset}'"></td> '
      echo '</tr>'
      echo -n " ";;

      CACHE_DRIVER) #https://stackoverflow.com/questions/45677563/laravel-which-cache-driver-to-use
      echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td><select id="'${entrkey}'" name="'${entrkey}'" > <option value="redis">redis (default integrated)</option><option value="database">database</option><option value="memcached">memcached</option><option value="file">file(Fallback)</option><option value="array">array (Debug)</option></select></td></tr>'
      echo -n " ";;

      SESSION_DRIVER) #https://stackoverflow.com/questions/17070851/laravel-session-drivers
      echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td><select id="'${entrkey}'" name="'${entrkey}'" ><option value="redis" selected>redis (default)</option><option value="memcached">memcached</option> <option value="cookie">cookie</option><option value="database">database</option><option value="native">native PHP (Fallback)</option><option value="array">array(DEBUG, non-persistent)</option></select></td></tr>'
      echo -n " ";;

      QUEUE_DRIVER)  #preferring database over sync over redis https://laracasts.com/discuss/channels/laravel/which-queue-driver-is-faster-database-or-redis
      echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td><select id="'${entrkey}'" name="'${entrkey}'" > <option value="database">database (default)</option> <option value="sync">sync</option> <option value="redis">redis</option></select></td></tr>'
      echo -n " ";;

      COMPOSE_PROJECT_NAME )
      targetname=${projectspace}/${domain}
      echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td><select id="'${entrkey}'" name="'${entrkey}'" > <option value="'${domain//./}'">'${domain//./}' (site name)</option><option value="'${projectspace}'">'${projectspace}' (project/customer name)</option></select></td></tr>'
      echo -n " ";;


#######


    APT_HTTP_PROXY_URL )
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

    APP_APTPROXY|ENABLE_WWW_SHELL)
    [ "${entrkey}" = "ENABLE_WWW_SHELL" ] && entrval=true
    if [ "${entrval}" = "true" ];then
      opposite=false
    else
      opposite=true
    fi
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td><select id="'${entrkey}'" name="'${entrkey}'" > <option value="'${entrval}'" selected>'${entrval}'</option><option value="'${opposite}'">'${opposite}'</option></select></td></tr>'
    echo -n " ";;

    PHP_VERSION)
#    [ "${entrkey}" = "ENABLE_WWW_SHELL" ] && entrval=true
 #   if [ "${entrval}" = "true" ];then
 #     opposite=false
 #   else
 #     opposite=true
 #   fi
 #   echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
 #                    <label for="'${entrkey}'">'${entrkey}'</label></td>
 #                        <td><select id="'${entrkey}'" name="'${entrkey}'" > <option value="'${entrval}'" selected>'${entrval}'</option><option value="'${opposite}'">'${opposite}'</option></select></td></tr>'

    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td><td><select id="'${entrkey}'" name="'${entrkey}'" >'
for PHPVERS in 5.6 7.2 7.4 8.0 8.1;do 
                        echo -n ' <option value="'${PHPVERS}'" '
                        [[ "$PHPVERS" = "${entrval}"  ]] && echo -n selected 
                        echo  ' >'${PHPVERS}'</option>'
done
echo '</select></td></tr>'


    echo -n " ";;

#mail adress checked with onblur

    MAIL_ADMINISTRATOR|LETSENCRYPT_EMAIL|WORDPRESS_ADMIN_MAIL|MAIL_FROM )
    THISMAILADMIN=$(cat /dev/shm/.env_admin_mail)
    preset="sysadmin@domain.lan"
    if [ ! -z $entrval ] ; then  preset=${entrval} ;fi
    if [ ! -z $THISMAILADMIN ] ; then  preset=${THISMAILADMIN} ;fi
    [ "${entrkey}" = "MAIL_FROM" ] && preset="no-reply@"$domain
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td style="min-width: 555px"><input style="width: 99%" type="email" pattern=".+@+.+" onblur="validateEmail(this);" id="'${entrkey}'" name="'${entrkey}'" value="'${preset}'" ></td>'
    [ "${entrkey}" = "MAIL_ADMINISTRATOR" ] && echo '  <td class="border border-opacity-75 border-black rounded-lg"> cronjobs and other container related things are sent here </td>'
    [ "${entrkey}" = "LETSENCRYPT_EMAIL" ] && echo '   <td class="border border-opacity-75 border-black rounded-lg"> used for certificate generation and renewal mails </td>'
    [ "${entrkey}" = "WORDPRESS_ADMIN_MAIL" ] && echo '<td class="border border-opacity-75 border-black rounded-lg"> mail adress of wordpress main user </td>'
    [ "${entrkey}" = "MAIL_FROM" ] && echo '           <td class="border border-opacity-75 border-black rounded-lg"> the adress that will be set if no from is set (e.g. cronjobs ) </td>'

    echo '</tr>'
    echo -n " ";;

    *_PASSWORD )
    dummy_preset="asdasdasdasdasdasd"
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td style="min-width: 555px"><input style="width: 99%" type="text" disabled="disabled" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${dummy_preset//?/.}'" autocomplete="off"></td></tr>'
    echo -n " ";;


    MAIL_USERNAME )
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td style="min-width: 555px"><input style="width: 99%"  id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td>'
    docker ps --format '{{.Names}} {{.Ports}}'|grep smtp|grep -e '->25/tcp' -e '->587/tcp' -q && {
    #echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">Hint:</td></tr>';
    mail_containers=$(docker ps --format '{{.Names}} {{.Ports}}'|grep smtp|grep -e '->25/tcp' -e '->587/tcp' |cut -f1 |cut -d " " -f1 )
    #echo "${mail_containers}"
    echo '<td class="border border-opacity-75 border-black rounded-lg text-xl">  for using internal smtp container(s)  <br>set  MAIL_USERNAME to :   <code>InternalNoTLSNoAuth</code><br> and MAIL_HOST to one of :  <code>'${mail_containers}'</code>  <br> or click here: <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded" onClick="dontSubmit(event);fillMailUser('"'"InternalNoTLSNoAuth"'"');fillMailHost('"'"$(echo "$mail_containers"|head -n1)"'"');return false;" onSubmit="dontSubmit(event); "  > Fill Values </button> </td>'
    echo -n ; } ;
    echo '</tr>'
    echo -n " ";;

    MAIL_DRIVER )
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" value="'${entrval}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    #echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg"><label for="'${entrkey}'">'${entrkey}'</label></td><td style="min-width: 555px"><input style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;


    MAIL_* )
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td style="min-width: 555px"><input style="width: 99%"  type="text" id="'${entrkey}'" name="'${entrkey}'" value="'${entrval}'" autocomplete="off"></td>'
    echo '</tr>'
    echo -n " ";;



    SSH_PORT )
      echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="ro'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input  style="width: 99%" disabled="disabled" type="text" id="ro-'${entrkey}'" name="ro-'${entrkey}'" value="'${random_ssh_port}'" placeholder="'${random_ssh_port}'" autocomplete="off"></td>'
      #disabled so we need hidden for submission
      echo '<td><input type="hidden" id="'${entrkey}'" name="'${entrkey}'" value="'${random_ssh_port}'"> </td>'
      echo '</tr>'
    echo -n " ";;


    MYSQL_HOST|MARIADB_HOST )
    echo '<tr style="display: none;" ><td> <input  type="hidden" id="'${entrkey}'" name="'${entrkey}'" value="'${entryval}'"></td></tr>'
    echo -n " ";;
####### greyed out

    *_PORT )
      echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                       <label for="'${entrkey}'">'${entrkey}'</label></td>
                           <td style="min-width: 555px"><input  style="width: 99%" disabled="disabled" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;


###### hidden options ( since user shall not screw it up)

    #MARIADB_ROOT* ) echo -n " ";;
    #APP_URL ) echo -n " ";;
    HOSTNAMES ) echo -n " ";;
    SSL_POLICY ) echo -n " ";;
    #STORAGE_ROOT ) echo -n " ";;
    #  *_PORT ) echo -n " ";;
    INSTALL_* ) echo -n " ";;
    VIRTUAL_P* ) echo -n " ";;
    LETSENCRYPT_HOST ) echo -n " ";;

    *)
    echo '<tr class="border border-opacity-75 border-black rounded-lg"  ><td class="border border-opacity-75 border-black rounded-lg">
                     <label for="'${entrkey}'">'${entrkey}'</label></td>
                         <td style="min-width: 555px"><input style="width: 99%" type="text" id="'${entrkey}'" name="'${entrkey}'" placeholder="'${entrval}'" autocomplete="off"></td></tr>'
    echo -n " ";;

esac
done;IFS="$realifs"
echo '</tbody></table><button  class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" onClick="validateMailPass();validateEmailByID('"'"MAIL_FROM"'"'));validateEmailByID('"'"LETSENCRYPT_EMAIL"'"');validateEmailByID('"'"MAIL_ADMINISTRATOR"'"'));" type="submit" value="post">Review and confirm changes</button></center>'


##### ENV PARSING END

    echo '</table></center></div>'
    echo '<input  type="hidden" id="domain"        name="domain"        value="'${domain}'">'
    echo '<input  type="hidden" id="APP_URL"       name="APP_URL"       value="'${domain}'">'
    echo '<input  type="hidden" id="projectspace"  name="projectspace"  value="'${projectspace}'">'
    echo '<input  type="hidden" id="containertype" name="containertype" value="'${containertype}'">'
## TODO: compose file selection
    echo '<input  type="hidden" id="composefile"   name="composefile"   value="docker-compose-multi.yml">'
    echo '</form>'
else
  _echo_redbox "pre-checks failed"
fi
echo '</body></html>'
    exit 0
fi

#### creation step 3

#####
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=deploy' ]] ;then

echo "$html_header" "$javascript_validate_functions" "$js_live_love_log"'</head><body>'
[ "$DEBUGME" = "TRUE" ] && echo debug '<center><h1>Domain creation '$domain'</h1></center><br>'

checks_ok=yes;
##incoming params:
containertype="${param[containertype]}" # ( git repo -> check against reg)
domain="${param[domain]}" #
projectspace="${param[projectspace]}";# ( composeroot subfolder )
composefile="${param[composefile]}";# ( composeroot subfolder )


  giturl=${containertype};
  repofolder=/dev/shm/docker_container_lib/${giturl//*\//};
  [ "$DEBUGME" = "TRUE" ] && echo debug repofolder ${repofolder}
  ls -1 ${repofolder}/docker-compose.yml ${repofolder}/docker-compose-single.yml ${repofolder}/docker-compose-multi.yml 2>/dev/null  |grep "docker-compose"|grep yml -q && test -f ${repofolder}/DOTenv.example && { _echo_bluebutton "template is valid" ; }  || { _echo_redbox "Template invalid" ;checks_ok=no  ; } ;


  targetfolder=${COMPOSE_ROOT}/${projectspace}/${domain}

  test -e ${COMPOSE_ROOT}/${projectspace}  || { _echo_redbox  "7xx NO PROJECTFOLDER:<br>CANNOT WRITE ON NONEXISTING FOLDER: ${COMPOSE_ROOT}/${projectspace} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## check if project  folder is  writable
  test -e ${COMPOSE_ROOT}/${projectspace} && [ ! -w "${COMPOSE_ROOT}/${projectspace}" ] && { _echo_redbox  "7xx NO WRITE PERMISSIONS FOR PROJECTFOLDER:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## project exists, do nothing
  test -e ${COMPOSE_ROOT}/${projectspace}/${domain} && { _echo_redbox  "7xx ALREADY EXISTS:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  test -e ${COMPOSE_ROOT}/${projectspace}/${domain} && [ ! -w ${COMPOSE_ROOT}/${projectspace}}/${domain} ] &&  { _echo_redbox  "7xx NO PERMISSIONS BUT EXISTS:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;



## VERIFY MAIL CREDENTIALS



## curl test smtp
[ "$DEBUGME" = "TRUE" ] && echo debug mail ${param[MAIL_HOST]} ${param[MAIL_USERNAME]}

#curl -vk smtp://${MAIL_HOST}:587/   --mail-from "${MAIL_FROM}" --mail-rcpt "${MAIL_ADMINISTRATOR}"    --user "${MAIL_USER}":"${MAIL_PASSWORD}" --ssl  --upload-file /tmp/testmail.txt

test -d /dev/shm/emptycurldir || mkdir /dev/shm/emptycurldir
## attention , here we check 587 and most compose uses 25
[ ! -z "${param[MAIL_HOST]}" ] && [ ! "${param[MAIL_USERNAME]}" = "InternalNoTLSNoAuth" ] && {
  cd /dev/shm/emptycurldir ; 
  echo 'From: Docker-Deployer <'${param[MAIL_FROM]}'>
To: Admin User <'${param[MAIL_ADMINISTRATOR]}'>
Subject: Container Setup Test

Dear Admin,
This mail was sent to verify that your smtp settings for

    domain: '${param[APP_URL]}'
are working.

Regards
Docker-Deployer
____
| _ \  ___   ___| | _____ _ __ 
| | | |/ _ \ / __| |/ / _ \ __|
| |_| | (_) | (__|   <  __/ |   
|____/ \___/ \___|_|\_\___|_|   
                                
     _            _                         __  
  __| | ___ _ __ | | ___  _   _  ___ _ __   \ \ 
 / _` |/ _ \  _ \| |/ _ \| | | |/ _ \  __| (_) |
| (_| |  __/ |_) | | (_) | |_| |  __/ |     _| |
 \__,_|\___| .__/|_|\___/ \__, |\___|_|    ( ) |
           |_|            |___/            |/_/ 


                        ##         .
                  ## ## ##        ==
               ## ## ## ##       ===
           /"""""""""""""""""\___/ ===
      ~~~ {~~ ~~~~ ~~~ ~~~~ ~~~ ~ /  ===- ~~~
           \______ o           __/
             \    \         __/
              \____\_______/
              
 _ __ ___   __ _(_) |  ___ _   _  ___ ___ ___  ___ ___ / _|_   _| |
|  _ ` _ \ / _` | | | / __| | | |/ __/ __/ _ \/ __/ __| |_| | | | |
| | | | | | (_| | | | \__ \ |_| | (_| (_|  __/\__ \__ \  _| |_| | |
|_| |_| |_|\__,_|_|_| |___/\__,_|\___\___\___||___/___/_|  \__,_|_|


' > testmail.txt

  smtp_test_result=$(host "${param[MAIL_HOST]}" && curl -sS -vk smtp://"${param[MAIL_HOST]}":587/   --upload-file testmail.txt --mail-from "${param[MAIL_FROM]}" --mail-rcpt "${param[MAIL_ADMINISTRATOR]}"    --user "${param[MAIL_USERNAME]}":"${param[MAIL_PASSWORD]}" --ssl 2>&1 |sed 's/\r/\n/g' |grep -v ^$ |sed 's/\(^\|$\)/ | /g'|grep -e '<' -e '>'|grep -v -e "__docker_include" -e "\.sh" )
MAIL_SENT=FAIL
  echo "$smtp_test_result" | grep -e '^< 235' -e "235 Auth" |grep -i uth |grep -i ucce -q && MAIL_SENT=OK
  echo "$smtp_test_result" | grep -q '< 250 Ok' && MAIL_SENT=OK
  [[ "$MAIL_SENT" = "OK" ]] && { _echo_bluebutton "MAIL LOIGN OK"   ; } 
  [[ "$MAIL_SENT" = "OK" ]] || { _echo_redbox "MAIL LOGIN FAILED <br>"'<span class="text-xl">
                                                                    <code> '$(echo "$smtp_test_result" |tail -n 10 |sed 's/$/<br>/g')' </code>
                                                                    </span>MAIL LOGIN FAILED' ; } ; };




#TODO:single or multi decision

  if
   [ "$checks_ok" = "yes" ];then

  echo '<div id=reviewdeploy  class=" rounded-lg border border-opacity-75 border-blue-500 bg-gray-300 ml-auto flex-grow mr-auto self-center text-3xl">'
_echo_bluebox "Check values finally"
  echo '<form accept-charset=utf-8 action="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'?action=bootstrap_container" target="compose-iframe" method="post" autocomplete="off">
      <center><table  class=" border border-opacity-75 border-blue-500 bg-blue-200 ml-auto flex-grow mr-auto self-center text-xl" >
      <thead> <tr><td><center>'$(_echo_bluebutton Setting)'</center></td><td><center>'$(_echo_redbutton Value)'</center></td><td>'$(_echo_bluebox Hint)'</td> </thead>'

echo '<tr><td class=" text-white border border-opacity-75 border-blue-500"><center>'$(_echo_bluebox_m domain)'</center></td><td class=" border border-opacity-75 border-blue-500">'${domain}'</td></td><td class=" border border-opacity-75 border-blue-500">the chosen domain</td></tr>' ;
echo '<tr><td class=" text-white border border-opacity-75 border-blue-500"><center>'$(_echo_bluebox_m projectspace)'</center></td><td class=" border border-opacity-75 border-blue-500">'${projectspace}'</td></tr>';
envraw=$(cat ${repofolder}/DOTenv.example|grep -v '^#')
envindexes=$(echo "$envraw"|grep -v '^#'|cut -d= -f1)
#indexes="${!param[@]}"


## FILL PARAM WITH MISSING ENV
for curindex in $envindexes; do


[ ${param[$curindex]+abc} ] || { param[$curindex]=$(echo "$envraw"|grep "^${curindex}"|cut -d= -f2-) ; [ "$DEBUGME" = "TRUE" ] && echo "<tr><td>debug filled $curindex , is now "${param[$curindex]}"</td></tr>"  ; } ;

done


for index in "${!param[@]}"; do
    if [ ! -z "${param[$index]}" ] ; then
    #echo "${param[$index]}" |grep -q ^[A-Z] && echo "<tr><td>"${index}'</td><td>'${param[$index]}'</td>' ;
    echo -n '<tr><td class=" text-white border border-opacity-75 border-blue-500"><center>'$(_echo_bluebox_m ${index})'</center></td><td class=" border border-opacity-75 border-blue-500">'${param[$index]}'</td>' ;
    ##values will be sent 1:1 to next form
    echo -n '<td><input type="hidden" id="'$index'" name="'$index'" value="'${param[$index]}'"></td>'
    echo -n '</tr>';
    echo ;
  fi
done |sort -u

##echo '<input type="hidden" id="target" name="target" value="'${targetname}'">'
 #echo $POST_STRING
 logtarget="deploy-docker."$(date +%s.%N)".log"

   echo '<tr><td ><input type="hidden" id="logtarget" name="logtarget" value="'$logtarget'"> </td> </tr>'


     echo '<tr><td></td><td><button id=deploybutton class="flex-grow lg:flex lg:items-center lg:w-auto bg-blue-600 ml-auto mr-auto self-center lg:items-center hover:bg-red-400 text-white font-bold py-2 px-4 rounded-full" onclick="trigger_logtail(' "'"${logtarget}"'" ');document.getElementById('"'"reviewdeploy"'"').style.display = '"'"none"'"' ;document.getElementById('"'"reviewdeploy"'"').style.height = '"'"30px"'"' ;" type="submit" value="post">Deploy container</button></td><td><div id="jschecktxt"></div></tr>'
echo '</table></center></form></div>'




          echo '<div id="actiondiv" class="  table-fixed rounded-lg bg-blue-500 bg-opacity-25 " > <center><table  style="width: 99%" class="  table-fixed rounded-lg bg-blue-400 bg-opacity-25 ml-auto flex-grow mr-auto self-center text-3xl" ><thead><tr><th class="w-3/4 px-4 py-2"><h2 style="align: right">Console Log: </h2></center></th> <th  class="text-sm w-1/2 px-4 py-2"><div style="max-width: 333px" id="idlestatus"  class="blink-text rounded-full self-center text-center bg-opacity-25 text-black border-blue-200 bg-gray-500"> Status: Waiting...</div></th></tr></thead></table></center>
          <center>
          <div style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 423px;overflow:hidden;width: 90%" class=" rounded-lg border border-opacity-75 border-blue-600 bg-black ml-auto flex-grow mr-auto self-center">
          <div style="width: 95%" id="msgtxt" class=" rounded-lg border border-opacity-75 text-left text-white border-yellow-600 bg-gray-900 ml-auto flex-grow mr-auto self-center text-sm"></div></div><hr></center>
          <div class=" rounded-lg border border-opacity-75 border-blue-600 bg-blue-500 ml-auto flex-grow mr-auto self-center text-3xl"><br>
          <center><iframe style="margin-top:0px; border-radius: 35px; min-width: 999px;min-height: 523px;overflow:hidden;width: 90%" class="ml-auto rounded-lg flex-grow mr-auto self-center text-white border-blue-600 bg-gray-400 " name="compose-iframe" src="/cgi-bin/'$( basename "${BASH_SOURCE[0]}" )'"></iframe></center>
          </div></div>'

else
  _echo_redbox "pre-checks failed"
fi
echo '</body></html>'
echo -n;
fi


##################################################

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=bootstrap_container' ]] ;then
## this one has GET and POST
checks_ok=yes;
##incoming params:
containertype="${param[containertype]}" # ( git repo -> check against reg)
domain="${param[domain]}" #
projectspace="${param[projectspace]}";# ( composeroot subfolder )
composefile="${param[composefile]}";# ( composeroot subfolder )


#targetenv=$(cat /dev/shm/docker-envlist |grep "/${param[target]}/.env")
#targetname=${targetenv%\/.env}
#targetname=${targetname#\/}
#targetdir=$(dirname "${COMPOSE_ROOT}${targetenv}")
#targetfolder="${COMPOSE_ROOT}/${projectspace}/${domain}";

targetenv="${COMPOSE_ROOT}/${projectspace}/${domain}/.env"
targetdir="${COMPOSE_ROOT}/${projectspace}/${domain}";

echo "$html_header"'</head><body>
<div class=" border border-opacity-75 text-white border-blue-600 bg-gray-500 ml-auto flex-grow mr-auto self-center text-xl">'

  giturl=${containertype};
  repofolder=/dev/shm/docker_container_lib/${giturl//*\//};
  [ "$DEBUGME" = "TRUE" ] && echo debug repofolder ${repofolder}
  ls -1 ${repofolder}/docker-compose.yml ${repofolder}/docker-compose-single.yml ${repofolder}/docker-compose-multi.yml 2>/dev/null  |grep "docker-compose"|grep yml -q && test -f ${repofolder}/DOTenv.example && { _echo_bluebutton "template is valid" ; }  || { _echo_redbox "Template invalid" ;checks_ok=no  ; } ;

  test -e ${COMPOSE_ROOT}/${projectspace}  || { _echo_redbox  "NO PROJECTFOLDER:<br>CANNOT WRITE ON NONEXISTING FOLDER ${COMPOSE_ROOT}/${projectspace} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## check if project  folder is  writable
  test -e ${COMPOSE_ROOT}/${projectspace} && [ ! -w "${COMPOSE_ROOT}/${projectspace}" ] && { _echo_redbox  "NO WRITE PERMISSIONS FOR PROJECTFOLDER:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## project exists, do nothing
  test -e ${COMPOSE_ROOT}/${projectspace}/${domain} && { _echo_redbox  "NO WRITE PERMISSIONS:<br>ALREADY EXISTS ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;


echo "<center>logging to "${param[logtarget]}"</center>"


 (
 echo  "################### ^ STARTING ^ ###################" |yellow
      uptime | yellow
    indexmsg=""
    #echo -n "copy: ";cp -r ${repofolder} "${COMPOSE_ROOT}/${projectspace}/${domain}" 2>&1 && ln -s ${COMPOSE_ROOT}/${projectspace}/${domain}/${composefile} ${COMPOSE_ROOT}/${projectspace}/${domain}/docker-compose.yml
    echo -n "copy: ";cp -r ${repofolder} "${COMPOSE_ROOT}/${projectspace}/${domain}" 2>&1 && (cd ${COMPOSE_ROOT}/${projectspace}/${domain}/ && ln -s ${composefile} docker-compose.yml ) &
    echo_blue "#### generating.env "$COMPOSE_ROOT"/${targetdir#$COMPOSE_ROOT}/.env"
    for index in "${!param[@]}"; do
       if [[ "${index}" =~ ^[A-Z] ]] ;then
          echo -n $(echo " | setting $index "|yellow)
          echo "$index"="${param[$index]}" | tee -a "${targetdir}/.env" &>/dev/null
      fi
    done


test -e  ${targetdir} || echo "Something failed creating out target folder" |red
test -e  ${targetdir} && test -e ${targetdir}/.env || echo "failed to create .env file "|red
test -e  ${targetdir} && test -e ${targetdir}/.env && { 
    cd ${targetdir} ; pwd

    ### if "COMPOSE_PROJECT_NAME" was changed , we have to shutdown before
#######    echo ${POST_STRING}|grep -v "COMPOSE_PROJECT_NAME=&" |grep -q COMPOSE_PROJECT_NAME && _compose_down "${targetdir}" 2>&1

    echo;echo "################### ↓ FETCHING ↓ #####################"| yellow

    git config --global --add safe.directory ${targetdir}/
    cd ${targetdir}/ && git pull || true
    _compose_pull "${targetdir}"
    echo;echo "################### ↑ DEPLOYING ↑ ####################"| yellow
    echo "Please Standby for the containers to build (up to 30minutes). Logging to ${param[logtarget]}.build.log"
    echo "building..."
    docker-compose build --no-rm 2>&1 |tee /dev/shm/"${param[logtarget]}.build.log" |grep -e '/' |tr -d '\n'
    _compose_up "${targetdir}"  2>&1
    echo;echo "################### OPERATION FINISHED ###############"|green
     echo -n ; } ;
) |sed 's/\r/ /g'| _timestamper  | aha --no-header  |tee  /dev/shm/${param[logtarget]} 1>&2

#| while read -r line; do  strip_escape_codes "${line}" line_stripped;   echo "${line_stripped}" ;done | _timestamper  |tee  /dev/shm/${param[logtarget]} 1>&2
echo '<center><h3>Finished applying changes '"</h3></center>"
#echo $POST_STRING
echo '</div></body></html>'


fi

## ACTION TO ADD DOMAIN AS Proxy

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=bootstrap_proxydomain' ]] ;then
## this one has GET and POST
checks_ok=yes;
##incoming params:
#containertype="${param[containertype]}" # ( git repo -> check against reg)
domain="${param[domain]}" #
#projectspace="${param[projectspace]}";# ( composeroot subfolder )




#composefiletype="${param[composefile]}";# ( composeroot subfolder )
#[ -z "${composefiletype}" ] && composefile=docker-compose-multi.yml

giturl="$(cat /etc/docker_library.domainadd|head -n1|cut -f1)"
repofolder=/dev/shm/docker_container_lib/${giturl//*\//};

test -e ${repofolder} || git clone --recurse-submodules "${giturl}" "${repofolder}"

#targetenv="${COMPOSE_ROOT}/${param[target]}/.env"
sourceenv="${COMPOSE_ROOT}/${param[target]}"
## compose folder
sourcedir="${COMPOSE_ROOT}/${targetenv//\.env/}/"
## the "folder " above compose dir

projectspace=$(echo "${param[target]}"|sed 's/^\///g'|cut -d"/" -f1)

targetenv="${COMPOSE_ROOT}/${projectspace}/${param[domain]}/.env"
## compose folder
targetdir="${COMPOSE_ROOT}/${projectspace}/${param[domain]}/"
## the "folder " above compose dir




## try to detect VIRTUAL_HOST and VIRTUAL_PROTO of sourcedir

## in theory you need only one host since redirect magic happens in htaccess or app
## one could try target_hostname=$(grep VIRTUAL_HOST ${sourceenv}|cut -d= -f2 |sed 's/.\+,//g' )
## but we ASSUME APP_URL (!!)
source_hostname=$(grep APP_URL ${sourceenv}|cut -d"=" -f2)



composefile=docker-compose-${param[composefiletype]}-${param[TARGET_PROTO]}.yml



#param[APP_URL]=${param[domain]}
#param[TARGET_PORT]=${sourceport}
#param[TARGET_URL]=${source_hostname}



echo "$html_header"'</head><body>'
[ "$DEBUGME" = "TRUE" ] && echo "source_hostname $source_hostname targetenv ${targetenv} composefile ${composefile} sourceproto ${sourceproto} sourceport ${sourceport} repofolder ${repofolder} targetdir ${targetdir}"
[ "$DEBUGME" = "TRUE" ] && echo domain ${param[domain]}
echo '<div class=" border border-opacity-75 text-white border-blue-600 bg-gray-500 ml-auto flex-grow mr-auto self-center text-xl">'

#  giturl=${containertype};
#  repofolder=/dev/shm/docker_container_lib/${giturl//*\//};
  [ "$DEBUGME" = "TRUE" ] && echo debug repofolder ${repofolder}
  ls -1 ${repofolder}/docker-compose-{full,single}domain-http{,s}.yml  2>/dev/null  |grep "docker-compose"|grep yml -q && test -f ${repofolder}/DOTenv.example && { _echo_bluebutton "template is valid" ; }  || { _echo_redbox "Template invalid" ;checks_ok=no  ; } ;

  test -e ${COMPOSE_ROOT}/${projectspace}  || { _echo_redbox  "NO PROJECTFOLDER:<br>CANNOT WRITE ON NONEXISTING FOLDER ${COMPOSE_ROOT}/${projectspace} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## check if project  folder is  writable
  test -e ${COMPOSE_ROOT}/${projectspace} && [ ! -w "${COMPOSE_ROOT}/${projectspace}" ] && { _echo_redbox  "NO WRITE PERMISSIONS FOR PROJECTFOLDER:<br>CANNOT WRITE ON ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;

  ## project exists, do nothing
  test -e ${COMPOSE_ROOT}/${projectspace}/${domain} && { _echo_redbox  "NO WRITE PERMISSIONS:<br>ALREADY EXISTS ${COMPOSE_ROOT}/${projectspace}/${domain} as $(id -un) ($(id -u)) ";checks_ok=no ;  } ;


echo "<center>logging to "${param[logtarget]}"</center>"


 (
 echo  "################### ^ STARTING ^ ###################" |yellow
      uptime | yellow
    indexmsg=""
    #echo -n "copy: ";cp -r ${repofolder} "${COMPOSE_ROOT}/${projectspace}/${domain}" 2>&1 && ln -s ${COMPOSE_ROOT}/${projectspace}/${domain}/${composefile} ${COMPOSE_ROOT}/${projectspace}/${domain}/docker-compose.yml
    echo -n "copy: ";cp -r ${repofolder} "${COMPOSE_ROOT}/${projectspace}/${domain}" 2>&1 && (cd ${COMPOSE_ROOT}/${projectspace}/${domain}/ && ln -s "${composefile}" docker-compose.yml ) &

    echo_blue "#### generating .env "$COMPOSE_ROOT"/${targetdir#$COMPOSE_ROOT}/.env"
    for index in "${!param[@]}"; do
       if [[ "${index}" =~ ^[A-Z] ]] ;then
          echo -n $(echo " | setting $index "|yellow)
          echo "$index"="${param[$index]}" | tee -a "${targetdir}/.env" &>/dev/null
      fi
    done


test -e  ${targetdir} || echo "Something failed creating out target folder" |red
test -e  ${targetdir} && test -e ${targetdir}/.env || echo "failed to create .env file "|red
test -e  ${targetdir} && test -e ${targetdir}/.env && { cd ${targetdir} ; pwd

    ### if "COMPOSE_PROJECT_NAME" was changed , we have to shutdown before
#######    echo ${POST_STRING}|grep -v "COMPOSE_PROJECT_NAME=&" |grep -q COMPOSE_PROJECT_NAME && _compose_down "${targetdir}" 2>&1

    echo;echo "################### ↓ FETCHING ↓ #####################"| yellow
    cd ${targetdir}/ && git pull || true
    _compose_pull "${targetdir}"
    echo;echo "################### ↑ DEPLOYING ↑ ####################"| yellow
    echo "Please Standby for the containers to build (up to 30minutes). Logging to ${param[logtarget]}.build.log "
    echo "building..."
    docker-compose build --no-rm 2>&1 |tee /dev/shm/"${param[logtarget]}.build.log" |grep -e '/'
    _compose_up "${targetdir}"  2>&1
    echo;echo "################### OPERATION FINISHED ###############"|green
     echo -n ; } ;
) |sed 's/\r/ /g'| _timestamper  | aha --no-header  |tee  /dev/shm/${param[logtarget]} 1>&2

#| while read -r line; do  strip_escape_codes "${line}" line_stripped;   echo "${line_stripped}" ;done | _timestamper  |tee  /dev/shm/${param[logtarget]} 1>&2
echo '<center><h3>Finished applying changes '"</h3></center>"
#echo $POST_STRING
echo '</div></body></html>'


fi



## API REQUEST TO SEE IF DOMAIN POINTS TO US

if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=verifydomain' ]] ;then
  starttime=$(timestamp_nanos)
  echo "Content-type: text/plain"
  echo  ""
    domain="${param[domain]}" # this gets domain from post
  [ -z "$domain" ] &&  domain=$( echo  "${REQUEST_URI}" |sed 's/?/\n/g;s/&/\n/g'|grep ^domain=|head -n1 |cut -d"=" -f2 )
  [ -z "$domain" ] && { echo ' { "success": false, "msg": "domain not sent via get or post" }' ; exit 1;};

  dnsmsg=""
  checks_ok=yes

    host  "$domain" &>/dev/null  && { dnsmsg=${dnsmsg}" | Domain $domain does exist" ; }
    host  "$domain" &>/dev/null  || { dnsmsg=${dnsmsg}" | Domain does not exist " ; checks_ok=no ; }

    ## find ipv4 from the docker-mounted /realproc
    for myv4 in $(_nslookup_ip4 $domain);do 
    grep -q "$myv4" /realproc/net/fib_trie && { dnsmsg=${dnsmsg}" | IPv4 $myv4 of $domain points to us " ; } 
    grep -q "$myv4" /realproc/net/fib_trie || { dnsmsg=${dnsmsg}'\n'" | IPv4 $myv4 of $domain does NOT point to this machine " ; checks_ok=no ; } ;done
    #find ipv6 from the docker-mounted /realproc
    publicv6=$(for i in "$(grep /realproc/net/if_inet6 -v -e lo$ -e ^fe80  )"; do     echo "$i" | gawk '@include "join"
                                                                                                        {
                                                                                                            split($1, _, "[0-9a-f]{,4}", seps)
                                                                                                            print join(seps, 1, length(seps), ":")
                                                                                                        }'; done |sed 's/:0\+/:/g')

for myv6 in $(_nslookup_ip6 $domain|grep ":"|sed 's/:0\+/:/g');do
     [ "$DEBUGME" = "TRUE" ] && echo debug search ip "${myv6//::/:}"
     echo "${publicv6//:*/:}"|grep -q ^"${myv6//:*/:}"  && { dnsmsg=${dnsmsg}" | IPv6 $myv6 of $domain points to us " ; } 
     echo "${publicv6//:*/:}"|grep -q ^"${myv6//:*/:}"  || { dnsmsg=${dnsmsg}'\n'" | IPv6 $myv6 of $domain does NOT point to this machine ( PUB: ${publicv6//::*/::} ME: ${myv6//::*/::} ) " ; checks_ok=no ; } ;
   done
    [ "$DEBUGME" = "TRUE" ] && echo debug publicv6 "${publicv6//::/:}"  >&2 ;



endtime=$(timestamp_nanos)
duration=$(($endtime-$starttime))
duration=$(awk "BEGIN {printf d "${duration}"/1000/1000}")
[   "$checks_ok" = "no"  ] && { echo ' { "success": false, "time": "'${duration}' ms", "msg": "'${dnsmsg}'" }' ; exit 1 ; } ;
[   "$checks_ok" = "yes" ] && { echo ' { "success": true , "time": "'${duration}' ms", "msg": "'${dnsmsg}'" }' ; exit 0 ; } ;

fi


###### API REQUEST FOR CHECKING SPF
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=verifyspf' ]] ;then
  starttime=$(timestamp_nanos)
  echo "Content-type: text/plain"
  echo  ""
  domain="${param[domain]}" # this gets domain from post
  [ -z "$domain" ] &&  domain=$( echo  "${REQUEST_URI}" |sed 's/?/\n/g;s/&/\n/g'|grep ^domain=|head -n1 |cut -d"=" -f2 )
  [ -z "$domain" ] && { echo ' { "success": false, "msg": "domain not sent via get or post" }' ; exit 1;};
spf_ok="yes"
msgtxt=""
spf_count=$(_nslookup "$domain" TXT|grep "v=spf"|wc -l) ;
dns_ipv6=$(_nslookup_ip6 "$domain") ;
dns_ipv4=$(_nslookup_ip4 "$domain") ;
wait
[ "$DEBUGME" = "TRUE" ] && echo debug ipdetect  >&2 ;
#_ipv6_all_public_ips /realproc/net/if_inet6 ; _ipv4_all_public_ips /realproc/net/fib_trie

testips=$(
          ## ipv4
          awk '/32 host/ { print f } {f=$2}' <<< "$(</realproc/net/fib_trie)"|grep -v -e ^192\.168 -e ^10\. -e ^127\. -e 172\.16 -e 172\.17 -e 172\.18 -e 172\.19 -e 172\.20 -e 172\.21 -e 172\.22 -e 172\.23 -e 172\.24 -e 172\.25 -e 172\.26 -e 172\.27 -e 172\.28 -e 172\.29 -e 172\.30 -e 172\.31 -e 172\.32 -e 169.254 |awk '!x[$0]++'
          #ipv6
          for i in "$(grep /realproc/net/if_inet6 -v -e lo$ -e ^fe80  )"; do     echo "$i" | gawk '@include "join"
            {
              split($1, _, "[0-9a-f]{,4}", seps)
              print join(seps, 1, length(seps), ":")
            }'; done
          )

##  find :0000:0000 as well as ::
alltestips=$(( echo "$testips"| sed 's/:0000/:/g;s/::\+$/::/g';
               echo "$testips"| sed 's/:0\+/:/g;s/::\+$/::/g';
               echo "$testips"| sed 's/:0\+/:/g;s/::\+$/::/g;s/::\+/::/g';
               echo "$testips" ) |sort -u)
[ "$DEBUGMEHARDER" = "TRUE" ] && cat /realproc/net/fib_trie
[ "$DEBUGMEHARDER" = "TRUE" ] && cat /realproc/net/if_inet6
[ "$DEBUGME" = "TRUE" ] && echo debug ips $testips >&2
[[ -z "${dns_ipv4}" ]] && [[ -z "${dns_ipv6}" ]] &&      { msgtxt=${msgtxt}'\n'" | FAILED SPF :       found NO IPv4 and IPv6         for ${domain} | " ; spf_ok=no ; } ;

[   "$spf_count" = 1 ] &&  for externalip in ${dns_ipv4} ${dns_ipv6} ;do
    [ "$DEBUGME" = "TRUE" ] && echo debug verifyip ${externalip} >&2
    #check that the ip is present on host
    echo "${alltestips}" |grep -q "${externalip}" ||     { msgtxt=${msgtxt}'\n'" | FAILED SPF : cant find ${externalip} in interfaces ${alltestips} for ${domain} | " ; spf_ok=no ; } ;
     
    spf_output=$(spfquery  -ip=${externalip} -sender=no-reply@${domain} -debug 2>&1 | tee /dev/shm/_spfcheck.log  )
    echo "$spf_output"|grep -q "Received-SPF: pass" &&   { msgtxt=${msgtxt}'\n'" | PASSED SPF : tried    ${externalip} as sender     for ${domain} | " ; } ;
    echo "$spf_output"|grep -q "Received-SPF: pass" ||   { msgtxt=${msgtxt}'\n'" | FAILED SPF : tried    ${externalip} as sender     for ${domain} | " ; spf_ok=no ; } ;
    
done

[  ! "$spf_count" = 1 ] && { spf_ok=no;msgtxt="More or less than one v=spf record";  } ;
[    "$spf_count" = 0 ] && { spf_ok=no;msgtxt="NO v=spf record";   } ;

endtime=$(timestamp_nanos)
duration=$(($endtime-$starttime))
duration=$(awk "BEGIN {printf 2d "${duration}"/1000/1000}")
[   "$spf_ok" = "no"  ] && { echo ' { "success": false, "time": "'${duration}' ms", "msg": "'${msgtxt}'" }' ; exit 1 ; } ;
[   "$spf_ok" = "yes" ] && { echo ' { "success": true , "time": "'${duration}' ms", "msg": "'${msgtxt}'" }' ; exit 0 ; } ;

fi





###### API REQUEST FOR GENERATING FILEZILLA CONFIG
if [[ ${REQUEST_URI} =~ $( basename "${BASH_SOURCE[0]}" )'?action=filezilla' ]] ;then
  domain="${param[domain]}" # this gets domain from post
  [ -z "$domain" ] &&  domain=$( echo  "${REQUEST_URI}" |sed 's/?/\n/g;s/&/\n/g'|grep ^domain=|head -n1 |cut -d"=" -f2 )

  CURRENT_SSH_PORT=$(docker exec -t ${domain} printenv|grep ^SSH_PORT|cut -d"=" -f2|head -n1 |tr -d '\n')
  [ -z "$CURRENT_SSH_PORT" ] && { echo "Content-type: text/plain";echo;echo ' { "success": false, "msg": "SSH PORT NOT SET FOR '${domain}' ... " }' ; exit 1;};

  echo -e 'Content-Type:application/x-download\nContent-Disposition: attachment; filename=filezilla.'${domain}'.xml\n\n'
  echo '<?xml version="1.0" encoding="UTF-8"?>
<FileZilla3 version="3.46.3" platform="*nix">
<Servers>
	<Server>
		<Host>'${domain}'</Host>
		<Port>'${CURRENT_SSH_PORT}'</Port>
		<Protocol>1</Protocol>
		<Type>0</Type>
		<User>www-data</User>
		<Keyfile>/home/yourUsername/.ssh/id_rsa</Keyfile>
		<Logontype>5</Logontype>
		<TimezoneOffset>0</TimezoneOffset>
		<PasvMode>MODE_DEFAULT</PasvMode>
		<MaximumMultipleConnections>0</MaximumMultipleConnections>
		<EncodingType>Auto</EncodingType>
		<BypassProxy>0</BypassProxy>
		<Name>'${domain}'</Name>
		<Comments />
		<Colour>0</Colour>
		<LocalDir />
		<RemoteDir>1 0 3 var 3 www 4 html</RemoteDir>
		<SyncBrowsing>0</SyncBrowsing>
		<DirectoryComparison>0</DirectoryComparison>
	</Server>
</Servers>
</FileZilla3>'


fi

#[ "$DEBUGME" = "TRUE" ] && echo debug prefunc
