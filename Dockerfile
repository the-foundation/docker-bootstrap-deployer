FROM alpine:latest
RUN apk add git bash dnsmasq gawk coreutils lighttpd  lighttpd lighttpd-mod_webdav lighttpd-mod_auth nano net-tools bind-tools curl docker-compose docker apache2-utils openssl strace openssh-keygen aha && git clone https://gitlab.com/b4sh-stack/b4sh-l1b.git /etc/bashlib
RUN apk add libspf2 || true && apk add libspf2-tools || true
RUN which spfquery |grep spfquery
RUN deluser xfs || true && delgroup xfs ||true && delgroup www-data &&  adduser www-data -u 33 --disabled-password -g www-data
#RUN ls /root && find /etc/lighttpd -delete && mkdir -p /etc/lighttpd && delgroup www-data || true && addgroup www-data -g 33 &&  grep 33 /etc/* &&   adduser www-data --disabled-password --uid 33

RUN mkdir -p /realproc/net/
## not possible
#COPY /proc/net/if_inet6 /realproc/net/if_inet6

RUN echo be sure to run following command in compose-directory : cat /proc/net/if_inet6 > if_inet6
COPY if_inet6 /realproc/net/if_inet6
RUN if [ "$(cat /realproc/net/if_inet6 | grep -v lo$|wc -l)" = "0" ]; then echo YOU DID NOT UPDATE if_inet6 , please run the following command in compose-directory : cat /proc/net/if_inet6 > if_inet6  ;exit 1;false ; fi
RUN echo be sure to run following command in compose-directory : cat /proc/net/fib_trie > fib_trie
COPY fib_trie.example /realproc/net/fib_trie
COPY fib_trie /realproc/net/fib_trie
RUN if [ "$(md5sum /realproc/net/fib_trie | grep 9b391e3779e60bc38a314b314842a232|wc -l)" = "1" ]; then echo YOU DID NOT UPDATE fib_trie , please run the following command in compose-directory : cat /proc/net/fib_trie > fib_trie  ;exit 1;false ; fi

RUN /bin/bash -c 'echo "su -s /bin/bash www-data" > /usr/bin/wwwsh;chmod +x /usr/bin/wwwsh'

RUN ls /root && find /etc/lighttpd -delete && mkdir -p /etc/lighttpd

RUN mkdir /etc/lighttpd/ssl/ssl.cert -p && cd /etc/lighttpd/ssl/ssl.cert && openssl req -new -x509 -keyout /etc/lighttpd/ssl/ssl.cert/server.pem -out /etc/lighttpd/ssl/ssl.cert/server.pem -days 36500 -nodes -subj "/C=WO/ST=NSA/L=Sydney/O=Docker-Deployer/OU=server/CN=docker-deployer/emailAddress=donot@spam.me"
RUN chown lighttpd:lighttpd -R /etc/lighttpd/ssl -R && chmod 0600 /etc/lighttpd/ssl/ssl.cert && ls /etc/lighttpd/ssl/ssl.cert

COPY lighttpdcnf/lighttpd.conf  lighttpdcnf/mime-types.conf  lighttpdcnf/mod_cgi.conf /etc/lighttpd/
#
#RUN rm -rf /var/www/ && mkdir -p /var/www/html
#COPY html/ /var/www/html



RUN echo "make sure to add a valid htaccess file n compose directory ( e.g. with: htpasswd -cB htpasswd deployhtpassuser)"
#COPY htpasswd /etc/lighttpd/.htpasswd


RUN head -c 5 /dev/random > /deleteme && cd /etc/bashlib && git pull || true && rm /deleteme
COPY init.sh /
CMD /bin/bash /init.sh
#CMD /bin/bash -c "ls -lh1 /etc/lighttpd/ssl/ssl.cert; \
#chmod go-rw -R /var/www/html  /usr/lib/cgi-bin/ ; \
#chgrp lighttpd /dev/stderr /dev/stdout ; \
#chmod g+w /dev/stderr /dev/stdout ; \
#chmod u+x  /var/www/html ; \
#chmod u-w -R /var/www/html ; \
#chmod u+r -R /var/www/html ; \
#chown -R lighttpd:lighttpd /var/www/html /usr/lib/cgi-bin/ ; \
#chmod +x /usr/lib/cgi-bin/*.sh ; \
#lighttpd -f /etc/lighttpd/lighttpd.conf  -D"
