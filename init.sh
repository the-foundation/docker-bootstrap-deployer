#!/bin/bash
export HOME=/root
ls -lh1 /etc/lighttpd/ssl/ssl.cert;
echo "getting gid of docker socket"
mygid=$(stat -c %g /var/run/docker.sock )
env |grep -e COMPOSE_ROOT >/etc/outerenv
echo "mygid " $mygid
echo detecting group or adding
cat /etc/group|grep ".*:.*:$mygid:" || addgroup docker-access --gid $mygid
mygrpname=$(grep ".*:.*:$mygid:" /etc/group|cut -d: -f1)
echo "adding user to group"
adduser www-data $mygrpname
echo "detecting docker-compose folder group"

echo getting compose folder group
compose_group=$(stat -c %g ${COMPOSE_ROOT} |grep -v ^0$)
if [ -z "$compose_group" ];then
  echo "failed to detect group , root is not allowed, numeric value was: "$(stat -c %g ${COMPOSE_ROOT})
else
  mygid=$compose_group
  cat /etc/group|grep ".*:.*:$mygid:" || addgroup docker-compose --gid $mygid
  mygrpname=$(grep ".*:.*:$mygid:" /etc/group|cut -d: -f1)
  echo "adding user to group"
  adduser www-data $mygrpname
  chmod g+w ${COMPOSE_ROOT}
fi

printenv MAIL_ADMINISTRATOR > /dev/shm/.env_admin_mail
chmod a+r /dev/shm/.env_admin_mail

#deluser xfs || true && delgroup xfs ||true && sed 's/^xfs:.\+//g' /etc/passwd /etc/passwd- /etc/group -i

#delgroup www-data || true && addgroup www-data -g 33 &&  grep 33 /etc/* &&   adduser www-data --disabled-password --uid 33

while (true);do cd /etc/bashlib &&  git pull &>/dev/null ;sleep 600;done &
chgrp www-data /etc/ssh_key_registry
chmod g+rw /etc/ssh_key_registry -R
chmod go-rw -R /var/www/html  /usr/lib/cgi-bin/ ;
chgrp www-data /dev/stderr /dev/stdout ;
chmod g+w /dev/stderr /dev/stdout ;
chmod u+x  /var/www/html ;
#chmod u-w -R /var/www/html ;
chmod u+r -R /var/www/html ;
chown -R www-data:www-data /var/www/html /usr/lib/cgi-bin/ ;
chmod +x /usr/lib/cgi-bin/*.sh ;

mkdir -p /dev/shm/lihttpdcompress /var/lib/lighttpd/cache/ && ln -s /dev/shm/lihttpdcompress /var/lib/lighttpd/cache/compress && chown -R www-data:www-data /var/lib/lighttpd/cache/compress /dev/shm/lihttpdcompress
echo "pulling library in background" ;
( su -s /bin/bash -c "source /usr/lib/cgi-bin/__docker_include.sh ;_update_deployer_preset_library" www-data ;chown -R www-data:www-data /dev/shm/docker*)  &


for nameserver in 9.9.9.9 8.8.8.8 1.1.1.1 64.6.65.6 77.88.8.7 156.154.70.1 91.239.100.100 13.239.157.177 46.182.19.48  194.150.168.168 2a02:2970:1002::18 2620:fe::fe  2620:fe::fe:9 ;do
   echo nameserver ${nameserver} >>/etc/resolv.conf.dnsmasq
done

dnsmasq  --max-cache-ttl=30 --max-ttl=30 --neg-ttl=30 --no-hosts --no-daemon --resolv-file=/etc/resolv.conf.dnsmasq --stop-dns-rebind --rebind-localhost-ok --domain-needed   &

echo nameserver 127.0.0.1 >/etc/resolv.conf
lighttpd -f /etc/lighttpd/lighttpd.conf  -D
