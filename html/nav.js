

/// {"link":"/maintain", "linktext": "Maintainance"}

// Create XMLHttpRequest object.
var oXHR = new XMLHttpRequest();
var jdata ;
// Initiate request.
oXHR.onreadystatechange = reportStatus;
oXHR.open("GET", "nav.json", true);  // get json file.
oXHR.send();

function reportStatus() {
    if (oXHR.readyState == 4) {		// Check if request is complete.
        console.log(this.responseText);
        // Create an HTML table using response from server.
        NavBarFromJSON(JSON.parse(this.responseText));
    }
}

function NavBarFromJSON(input) {
    var appender='<ul class="rounded-l flex mr-2">'
    var mydiv = document.getElementById("navbar");
    for (var i = 0; i < input.length; i++){
        //document.write("<br><br>array index: " + i);
        var obj = input[i];
        if (obj.hasOwnProperty('link')) {
          if (obj.hasOwnProperty('linktext')) {
            // different style for the active page
            if (window.location.href.indexOf(document.location.host + obj.link) > -1) {
                 appender=appender + '  <li class="mr-6"><a class="text-center block border border-blue-500 rounded py-2 px-4 bg-blue-500 hover:bg-blue-700 text-white" href="' + obj.link + '">' + obj.linktext + '</a></li>'
                 } else {
                 appender=appender + '  <li class="mr-6"><a class="text-center block border border-white rounded hover:border-gray-200 text-blue-500 hover:bg-gray-200 py-2 px-4" href="' + obj.link + '">' + obj.linktext + '</a></li>'
                 }
        }
        }
        //for (var key in obj){
        //  var value = obj[key];
        //  document.write("<br> - " + key + ": " + value);
        //}
    }
    appender = appender + '</ul>'
    mydiv.innerHTML=appender;
}
