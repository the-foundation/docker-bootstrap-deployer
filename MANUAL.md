Docker Bootstrap Deployer Manual
===


## CREATE HOST

* assign AAAA/A Records and SPF if necessary, then click on "NEW" in the menu
   ![./MANUAL/C_1.png](./MANUAL/C_1.png)

* fill in SMTP values or click "fill" to use pre-set values if given
   ![./MANUAL/C_2.png](./MANUAL/C_2.png)
* Check your values finally
   ![./MANUAL/C_2.png](./MANUAL/C_3.png)
* watch system deploy
   ![./MANUAL/C_4.png](./MANUAL/C_4.png)
* if the deployment took too long (timeouts), use the "change" function , change no values and proceed , this will issue `docker-compose up internally`
