# Docker Bootstrap Deployer

Auto Deploy Docker containers from git container profiles that only need .env files


USERS: refer to MANUAL.md

## Library

the library files for regular containers has entries like this:

```
https://git-url/someone/myrepo.git\TABdescription May Have Spaces
```

where `\TAB` is a real tabstop like `echo -en https://gitlab.com/my-id/my-project'\t'`

## Mails

* the internal mail container is used without password by setting the user to  `InternalNoTLSNoAuth`
* the mail container is detected when it has **a name containing `smtp`** AND **exposes either port `25` or `587`**

you need a directory `/opt/docker-compose/webprojects` with a group other than root(0) owning it


## Connecting:
 unless you expose the container to nginx or directly , it is bound on port `44443` at `127.0.0.1` , so you need to do
 `ssh default-mail-not-set@using-fallback-default.slmail.me -L 127.0.0.1:44443:127.0.0.1:44443` to connect your browser to [https://127.0.0.1:44443](https://127.0.0.1:44443) 

---


## !! beware of data directories in compose-folder before doing this !!

example: `addgroup sysadmin-docker;chown -R /opt/docker-compose/webprojects ; chmod g+r /opt/docker-compose/webprojects`

for detection of the extenal ipv4 and ipv6 of the host , it is necessary to put the contents of /proc/net/fib_trie AND /proc/net/if_inet6 into the file named like this once
example: `cat /proc/net/if_inet6> if_inet6;cat /proc/net/fib_trie> fib_trie`

# Installing

## Quick setup:

### 1. get some nginx-letsencrypt-companion

e.g. :

```
git pull --recurse-submodules https://gitlab.com/the-foundation/flying-docker-compose-letsencrypt-nginx-proxy-companion.git /opt/docker-compose/general/nginx
cd /opt/docker-compose/general/nginx
nano .env
docker-compose up --build -d

```
### 2. If you need mail:
* get the reverse dns of your host and spin up a mailserver only reachable from inside ( or set relay as below )
* (EMERGENCY) SIMPLE  `docker-compose.yml` example :

```
version: '3.1'

services:
  smtp.host.name.of.your.machine:
    container_name: smtp.host.name.of.your.machine
    image: ixdotai/smtp
    restart: unless-stopped
#    volumes:
#     - /opt/nginx/certs/host.name.of.your.machine/fullchain.pem:/etc/fc.pem
#     - /opt/nginx/certs/host.name.of.your.machine/key.pem:/etc/ky.pem
    ports:
      - "${IP:-0.0.0.0}:${DOCKER_SMTP:-25}:25"
      - "${IP:-0.0.0.0}:${DOCKER_SMTP:-587}:587"
    environment:
#       KEY_PATH: /etc/ky.pem
#       CERTIFICATE_PATH: /etc/fc.pem
#       LETSENCRYPT_HOST: host.name.of.your.machine
       MAILNAME: reverse.dns.of.your.machine
       RELAY_NETWORKS: :192.168.0.0/24:10.0.0.0/16:172.16.0.0/12
#      APP_URL: ${APP_URL}

#    volumes:
#      - ./anyfile:/opt/anyfile:ro

```
### 3 (optional) get auxiliary containers
   | TYPE | REPO | SETUP
   |---|---|---|
   | TYPO3 | https://gitlab.com/the-foundation/docker-typo3-src | `mkdir -p /opt/docker-compose/general/ \|\| true  && cd /opt/docker-compose/general && git clone https://gitlab.com/the-foundation/docker-typo3-src.git && cd docker-typo3-src && docker-compose up --build -d ` |
   
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-bootstrap-deployer/README.md/logo.jpg" width="480" height="270"/></div></a>
